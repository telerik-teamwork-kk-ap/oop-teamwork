﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Task_Management_System.Core;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Commands.Create;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Tests.CommandsTests.CreateTests
{
    [TestClass]
    public class CreateFeedbackCommandTests
    {
        private Repository GetRepository()
        {
            Repository repository = new();
            repository.CreateFeedback("Feedback's title", "Your work on this project is great. Keep doing the same way. ;)",
                10, FeedbackStatus.Done);
            return repository;
        }
        private ICommand GetCommand(params string[] parameters)
        {
            Repository repository = this.GetRepository();
            IList<string> parametersList = new List<string>(parameters);
            ICommand command = new CreateFeedbackCommand(parametersList, repository);
            return command;
        }

        [TestMethod]
        public void Return_Info_When_Help_Is_Аsked()
        {
            //Arrange
            var sut = this.GetCommand("help");
            //Act, Assert

            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_ParametersAreLessThenExpected()
        {
            //Arrange
            var sut = this.GetCommand("Great job!", "Feedback's description.");
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Task_Title_Exists()
        {
            //Arrange
            var sut = this.GetCommand("Feedback's title", "Feedback time is a wonderful opportunity for Payback.Mu - ha - ha - ha!", "2", "Scheduled") ;
            sut.Execute();
        }

        [TestMethod]
        public void Return_Message_When_ParametersAreCorrect()
        {
            //Arrange
            var sut = this.GetCommand("PayBackTime", "Feedback time is a wonderful opportunity for Payback.Mu - ha - ha - ha!", "2", "Scheduled");
            //Act, Assert
            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }
    }
}
