﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Commands.Create;
using Task_Management_System.Core;
using Task_Management_System.Helpers;

namespace Task_Management_System.Tests.CommandsTests.CreateTests
{
    [TestClass]
    public class CreateMemberCommandTests
    {
        private Repository GetRepository()
        {
            Repository repository = new();
            repository.CreateMember("Daniel");
            return repository;
        }
        private ICommand GetCommand(params string[] parameters)
        {
            Repository repository = this.GetRepository();
            IList<string> parametersList = new List<string>(parameters);
            ICommand command = new CreateMemberCommand(parametersList, repository);
            return command;
        }

        [TestMethod]
        public void Return_Info_When_Help_Is_Аsked()
        {
            //Arrange
            var sut = this.GetCommand("help");
            //Act, Assert

            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_ParametersAreLessThenExpected()
        {
            //Arrange
            var sut = this.GetCommand();
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Member_Name_Exists()
        {
            //Arrange
            var sut = this.GetCommand("Daniel");
            sut.Execute();
        }

        [TestMethod]
        public void Return_Message_When_ParametersAreCorrect()
        {
            //Arrange
            var sut = this.GetCommand("Stanimir");
            //Act, Assert
            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

    }
}
