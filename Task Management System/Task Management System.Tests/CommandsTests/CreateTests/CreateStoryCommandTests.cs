﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Task_Management_System.Core;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Commands.Create;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Tests.CommandsTests.CreateTests
{
    [TestClass]
    public class CreateStoryCommandTests
    {
        private Repository GetRepository()
        {
            Repository repository = new();
            repository.CreateStory("A Beautiful Day", "I hope this morning the weather outside is warm and sunny. If it is so I will go for a walk. But if it is not, I will stay at home.",
               Priority.High, StoryStatus.NotDone, StorySize.Small );
            return repository;
        }
        private ICommand GetCommand(params string[] parameters)
        {
            Repository repository = this.GetRepository();
            IList<string> parametersList = new List<string>(parameters);
            ICommand command = new CreateStoryCommand(parametersList, repository);
            return command;
        }

        [TestMethod]
        public void Return_Info_When_Help_Is_Аsked()
        {
            //Arrange
            var sut = this.GetCommand("help");
            //Act, Assert

            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_ParametersAreLessThenExpected()
        {
            //Arrange
            var sut = this.GetCommand("Story's title");
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Task_Title_Exists()
        {
            //Arrange
            var sut = this.GetCommand("A Beautiful Day", "Wonderful opportunity for a walk. Let's get started.", "Medium", "InProgress", "Large");
            sut.Execute();
        }

        [TestMethod]
        public void Return_Message_When_ParametersAreCorrect()
        {
            //Arrange
            var sut = this.GetCommand("Time to go out", "Wonderful opportunity for a walk. Let's get started.", "Medium", "InProgress", "Large");
            //Act, Assert
            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

    }
}
