﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Task_Management_System.Core;
using Task_Management_System.Commands.Create;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Tests.CommandsTests.CreateTests
{
    [TestClass]
    public class CreateBugCommandTests
    {
        private Repository GetRepository()
        {
            Repository repository = new();
            List<string> steps = new() { "1. Open the application","2. Click 'Log In'", "3. The application freezes!"};
            repository.CreateBug("The program freezes", "This needs to be fixed quickly!", Priority.High, Severity.Critical, steps);

            return repository;
        }
        private ICommand GetCommand(params string[] parameters)
        {
            Repository repository = this.GetRepository();
            IList<string> parametersList = new List<string>(parameters);
            ICommand command = new CreateBugCommand(parametersList, repository);
            return command;
        }

        [TestMethod]
        public void Return_Info_When_Help_Is_Аsked()
        {
            //Arrange
            var sut = this.GetCommand("help");
            //Act, Assert

            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_ParametersAreLessThenExpected()
        {
            //Arrange
            var sut = this.GetCommand("Tricky Bug");
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Task_Title_Exists()
        {
            //Arrange
            var sut = this.GetCommand("The program freezes", "Do something, please! It is very important!", "High", "Critical", "1, 2, 3.");
            sut.Execute();
        }

        [TestMethod]
        public void Return_Message_When_ParametersAreCorrect()
        {
            //Arrange
            var sut = this.GetCommand("Unpleasant Happening", "Do something, please! It is very important!", "High", "Critical", "1, 2, 3.");
            //Act, Assert

            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

    }
}
