﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Commands.View;
using Task_Management_System.Core;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Tests.CommandsTests.ViewTests
{
    [TestClass]
    public class ViewTeamBoardsCommandTests
    {
        [TestMethod]
        public void Throw_When_InvalidParameterCount()
        {
            //Arrange
            ICommand command = this.GetCommand();
            //Act and Assert
            Assert.ThrowsException<InvalidUserInputException>(() => command.Execute());
        }

        [TestMethod]
        public void Execute_When_OnlyParameterIsHelp()
        {
            //Arrange
            ICommand command = this.GetCommand("help");
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        public void Throw_When_TeamNotFound()
        {
            //Arrange
            ICommand command = this.GetCommand("not a team name");
            //Act and Assert
            Assert.ThrowsException<EntityNotFoundException>(() => command.Execute());
        }

        [TestMethod]
        public void Execute_When_ValidParameters()
        {
            //Arrange
            ICommand command = this.GetCommand("team name");
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        private IRepository GetRepository()
        {
            IRepository result = new Repository();
            var team = result.CreateTeam("team name");
            var board = result.CreateBoard(team, "board name");
            team.AddBoard(board);
            return result;
        }

        private ICommand GetCommand(params string[] arguments)
        {
            var repository = this.GetRepository();
            List<string> parameters = new(arguments);
            ICommand command = new ViewTeamBoardsCommand(parameters, repository);
            return command;
        }
    }
}
