﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Commands.View;
using Task_Management_System.Core;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Tests.CommandsTests.ViewTests
{
    [TestClass]
    public class ViewMemberCommandTests
    {
        [TestMethod]
        public void Throw_When_InalidParameterCount()
        {
            //Arrange
            ICommand command = this.GetCommand();
            //Act and Assert
            Assert.ThrowsException<InvalidUserInputException>(() => command.Execute());
        }

        [TestMethod]
        public void Execute_When_OnlyParameterIsHelp()
        {
            //Arrange
            ICommand command = this.GetCommand("help");
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        public void Execute_When_ValidParameters()
        {
            //Arrange
            ICommand command = this.GetCommand("member");
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        public void Throw_When_MemberNotFound()
        {
            //Arrange
            ICommand command = this.GetCommand("yanagi");
            //Act and Assert
            Assert.ThrowsException<EntityNotFoundException>(() => command.Execute());
        }

        private IRepository GetRepository()
        {
            IRepository result = new Repository();
            result.CreateMember("member");
            return result;
        }

        private ICommand GetCommand(params string[] arguments)
        {
            var repository = this.GetRepository();
            List<string> parameters = new(arguments);
            ICommand command = new ViewMemberCommand(parameters, repository);
            return command;
        }
    }
}
