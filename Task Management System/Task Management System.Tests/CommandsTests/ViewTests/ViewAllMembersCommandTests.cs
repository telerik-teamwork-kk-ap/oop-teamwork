﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Commands.View;
using Task_Management_System.Core;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;


namespace Task_Management_System.Tests.CommandsTests.ViewTests
{
    [TestClass]
    public class ViewAllMembersCommandTests
    {
        [TestMethod]
        public void Executes()
        {
            //Arrange
            ICommand command = this.GetCommand();
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        private IRepository GetRepository()
        {
            IRepository result = new Repository();
            return result;
        }

        private ICommand GetCommand()
        {
            var repository = this.GetRepository();
            ICommand command = new ViewAllMembersCommand(repository);
            return command;
        }
    }
}
