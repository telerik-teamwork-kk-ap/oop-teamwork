﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Commands.Add;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Core;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Tests.CommandsTests.AddTests
{
    [TestClass]
    public class AddTaskToBoardCommandTests
    {
        private Repository GetRepository()
        {
            Repository repository = new();
            var team = repository.CreateTeam("First Team");
            var board = repository.CreateBoard(team, "My Board");
            repository.CreateFeedback("Feedback Task", "Description, description, description", 1, FeedbackStatus.Scheduled);
            var task2 = repository.CreateFeedback("Task2 - Feedback", "Description, description, description", 2, FeedbackStatus.Unscheduled);
            board.AddTask(task2);
            return repository;
        }
        private ICommand GetCommand(params string[] parameters)
        {
            Repository repository = this.GetRepository();
            IList<string> parametersList = new List<string>(parameters);
            ICommand command = new AddTaskToBoardCommand(parametersList, repository);
            return command;
        }

        [TestMethod]
        public void Return_Info_When_Help_Is_Аsked()
        {
            //Arrange
            var sut = this.GetCommand("help");
            //Act, Assert
            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_ParametersAreLessThenExpected()
        {
            //Arrange
            var sut = this.GetCommand("First Team");
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public void Should_Throw_When_Task_Does_Not_Exist()
        {
            //Arrange
            var sut = this.GetCommand("New Task 123", "My Board");
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public void Should_Throw_When_Board_Does_Not_Exist()
        {
            //Arrange
            var sut = this.GetCommand("Feedback Task", "Board N2");
            sut.Execute();
        }

        [TestMethod]
        public void Return_Message_When_This_Board_Already_Has_This_Task()
        {
            //Arrange
            var sut = this.GetCommand("Task2 - Feedback", "My Board");
            //Assert
            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        public void Return_Correct_Message_When_This_Team_Already_Has_This_Member()
        {
            //Arrange
            var sut = this.GetCommand("Task2 - Feedback", "My Board");
            string text = "Board 'My Board' already has task 'Task2 - Feedback'.";
            //Assert
            Assert.AreEqual(text, sut.Execute());
        }

        [TestMethod]
        public void Return_Message_When_ParametersAreCorrect()
        {
            //Arrange
            var sut = this.GetCommand("Feedback Task", "My Board");
            //Assert
            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        public void Return_Correct_Message_When_ParametersAreCorrect()
        {
            //Arrange
            var sut = this.GetCommand("Feedback Task", "My Board");
            string text = "A new task has been added to board 'My Board'.";
            //Assert
            Assert.AreEqual(text, sut.Execute());
        }
    }
}
