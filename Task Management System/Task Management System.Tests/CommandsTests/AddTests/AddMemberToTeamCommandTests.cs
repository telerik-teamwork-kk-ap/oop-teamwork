﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Commands.Add;
using Task_Management_System.Helpers;

namespace Task_Management_System.Tests.CommandsTests.AddTests
{
    [TestClass]
    public class AddMemberToTeamCommandTests
    {
        private Repository GetRepository()
        {
            Repository repository = new();
            var team = repository.CreateTeam("Big Players");
            var member = repository.CreateMember("Katerina");
            var member2 = repository.CreateMember("Paraskeva");
            team.AddMember(member2);
            return repository;
        }
        private ICommand GetCommand(params string[] parameters)
        {
            Repository repository = this.GetRepository();
            IList<string> parametersList = new List<string>(parameters);
            ICommand command = new AddMemberToTeamCommand(parametersList, repository);
            return command;
        }

        [TestMethod]
        public void Return_Info_When_Help_Is_Аsked()
        {
            //Arrange
            var sut = this.GetCommand("help");
            //Act, Assert
            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_ParametersAreLessThenExpected()
        {
            //Arrange
            var sut = this.GetCommand("Big Players");
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public void Should_Throw_When_Team_Does_Not_Exist()
        {
            //Arrange
            var sut = this.GetCommand("SmallLeague", "Katerina");
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public void Should_Throw_When_Member_Does_Not_Exist()
        {
            //Arrange
            var sut = this.GetCommand("Big Players", "Nataliya");
            sut.Execute();
        }

        [TestMethod]
        public void Return_Message_When_This_Team_Already_Has_This_Member()
        {
            //Arrange
            var sut = this.GetCommand("Big Players", "Paraskeva");
            //Act, Assert
            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        public void Return_Correct_Message_When_This_Team_Already_Has_This_Member()
        {
            //Arrange
            var sut = this.GetCommand("Big Players", "Paraskeva");
            string text = "Team Big Players already has member Paraskeva.";
            //Act, Assert
            Assert.AreEqual(text, sut.Execute());
        }

        [TestMethod]
        public void Return_Message_When_ParametersAreCorrect()
        {
            //Arrange
            var sut = this.GetCommand("Big Players", "Katerina");
            //Act, Assert
            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        public void Return_Correct_Message_When_ParametersAreCorrect()
        {
            //Arrange
            var sut = this.GetCommand("Big Players", "Katerina");
            string text = "A new member has been added to team 'Big Players'.";
            //Act, Assert
            Assert.AreEqual(text, sut.Execute());
        }
    }
}
