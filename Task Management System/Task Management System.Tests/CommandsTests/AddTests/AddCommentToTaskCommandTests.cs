﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Commands.Add;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Core;
using Task_Management_System.Models.Enums;
using Task_Management_System.Helpers;

namespace Task_Management_System.Tests.CommandsTests.AddTests
{
    [TestClass]
    public class AddCommentToTaskCommandTests
    {
        private Repository GetRepository()
        {
            Repository repository = new();
            var task = repository.CreateFeedback("Feedback task", "This is the smallest task I could choose.", 4, FeedbackStatus.New);
            var member = repository.CreateMember("Katerina");
            return repository;
        }
        private ICommand GetCommand(params string[] parameters)
        {
            Repository repository = this.GetRepository();
            IList<string> parametersList = new List<string>(parameters);
            ICommand command = new AddCommentToTaskCommand(parametersList, repository);
            return command;
        }

        [TestMethod]
        public void Return_Info_When_Help_Is_Аsked()
        {
            //Arrange
            var sut = this.GetCommand("help");
            //Act, Assert

            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_ParametersAreLessThenExpected()
        {
            //Arrange
            var sut = this.GetCommand("Feedback task", "Katerina");
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public void Should_Throw_When_Task_Does_Not_Exist()
        {
            //Arrange
            var sut = this.GetCommand("New feedback task", "Katerina", "Katerina it is always pleasure to work with you!");
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public void Should_Throw_When_Member_Does_Not_Exist()
        {
            //Arrange
            var sut = this.GetCommand("Feedback task", "Nataliya", "Nataliya it is always pleasure to work with you!");
            sut.Execute();
        }

        [TestMethod]
        public void Return_Message_When_ParametersAreCorrect()
        {
            //Arrange
            var sut = this.GetCommand("Feedback task", "Katerina", "Katerina it is always pleasure to work with you!");
            //Act, Assert
            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }
    }
}
