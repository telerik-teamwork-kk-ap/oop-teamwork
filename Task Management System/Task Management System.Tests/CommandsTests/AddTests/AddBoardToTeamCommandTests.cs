﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Task_Management_System.Commands.Add;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Core;
using Task_Management_System.Helpers;

namespace Task_Management_System.Tests.CommandsTests.AddTests
{
    [TestClass]
    public class AddBoardToTeamCommandTests
    {
        private Repository GetRepository()
        {
            Repository repository = new();
            var team1 = repository.CreateTeam("Winners");
            var team2= repository.CreateTeam("Loosers");
            var board1 = repository.CreateBoard(team1, "Work!");
            return repository;
        }
        private ICommand GetCommand(params string[] parameters)
        {
            Repository repository = this.GetRepository();
            IList<string> parametersList = new List<string>(parameters);
            ICommand command = new AddBoardToTeamCommand(parametersList, repository);
            return command;
        }

        [TestMethod]
        public void Return_Info_When_Help_Is_Аsked()
        {
            //Arrange
            var sut = this.GetCommand("help");
            //Act, Assert

            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_ParametersAreLessThenExpected()
        {
            //Arrange
            var sut = this.GetCommand("Winners");
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public void Should_Throw_When_Team_Does_Not_Exist()
        {
            //Arrange
            var sut = this.GetCommand("Friends", "Work!");
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public void Should_Throw_When_Board_Does_Not_Exist()
        {
            //Arrange
            var sut = this.GetCommand("Winners", "ForPrint");
            sut.Execute();
        }

        [TestMethod]
        public void Should_Return_Message_When_This_Board_Is_Already_Added_To_This_Team()
        {
            //Arrange
            var sut = this.GetCommand("Winners", "Work!");
            //Act, Assert
            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        public void Return_Message_When_ParametersAreCorrect()
        {
            //Arrange
            var sut = this.GetCommand("Loosers", "Work!");
            //Act, Assert
            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

    }
}
