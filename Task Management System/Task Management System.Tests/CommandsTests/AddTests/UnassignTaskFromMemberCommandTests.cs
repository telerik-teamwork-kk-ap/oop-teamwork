﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Commands.Add;
using Task_Management_System.Models.Enums;
using Task_Management_System.Helpers;

namespace Task_Management_System.Tests.CommandsTests.AddTests
{
    [TestClass]
    public class UnassignTaskFromMemberCommandTests
    {
        private Repository GetRepository()
        {
            Repository repository = new();
            var member = repository.CreateMember("Dragostin");
            var task = repository.CreateStory("Story's name3", "Description, description, description", Priority.Low, StoryStatus.InProgress, StorySize.Small);
            member.AddTask(task);
            repository.CreateStory("Story's name", "Description, description, description", Priority.Low, StoryStatus.InProgress, StorySize.Small);
            return repository;
        }
        private ICommand GetCommand(params string[] parameters)
        {
            Repository repository = this.GetRepository();
            IList<string> parametersList = new List<string>(parameters);
            ICommand command = new UnassignTaskFromMemberCommand(parametersList, repository);
            return command;
        }

        [TestMethod]
        public void Return_Info_When_Help_Is_Аsked()
        {
            //Arrange
            var sut = this.GetCommand("help");
            //Act, Assert
            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_ParametersAreLessThenExpected()
        {
            //Arrange
            var sut = this.GetCommand("Dragostin");
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public void Should_Throw_When_Member_Does_Not_Exist()
        {
            //Arrange
            var sut = this.GetCommand("Stiliyan", "Feedback Task");
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public void Should_Throw_When_Task_Does_Not_Exist_In_This_Member_Tasks()
        {
            //Arrange
            var sut = this.GetCommand("Dragostin", "Story or Bug");
            sut.Execute();
        }


        [TestMethod]
        public void Return_Message_When_ParametersAreCorrect()
        {
            //Arrange
            var sut = this.GetCommand("Dragostin", "Story's name3");
            //Assert
            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        public void Return_Correct_Message_When_ParametersAreCorrect()
        {
            //Arrange
            var sut = this.GetCommand("Dragostin", "Story's name3");
            string text = $"Task 'Story's name3' was succsefully unassigned from Dragostin.";
            //Assert
            Assert.AreEqual(text, sut.Execute());
        }
    }
}
