﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Commands.Modify;
using Task_Management_System.Core;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Tests.CommandsTests.ModifyTests
{
    [TestClass]
    public class ChangeBugStatusCommandTests
    {
        [TestMethod]
        public void Throw_When_InvalidParameterCount()
        {
            //Arrange
            ICommand command = this.GetCommand("1");
            //Act and Assert
            Assert.ThrowsException<InvalidUserInputException>(() => command.Execute());
        }

        [TestMethod]
        public void Execute_When_OnlyParameterIsHelp()
        {
            //Arrange
            ICommand command = this.GetCommand("help");
            //Act 
            string mes = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(mes));
        }

        [TestMethod]
        public void Throw_When_BugIsNotFound()
        {
            //Arrange
            ICommand command = this.GetCommand("123", "fixed");
            //Act and Assert
            Assert.ThrowsException<EntityNotFoundException>(() => command.Execute());
        }

        [TestMethod]
        [DataRow("active")]
        [DataRow("fixed")]
        public void Execute_When_ValidParameters(string status)
        {
            //Arrange
            ICommand command = this.GetCommand("Valid title", status);
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        private IRepository GetRepository()
        {
            IRepository result = new Repository();
            result.CreateBug("Valid title", "Valid Description", Priority.High, Severity.Major, new List<string> { "open", "use", "close" });
            return result;
        }

        private ICommand GetCommand(params string[] arguments)
        {
            var repository = this.GetRepository();
            List<string> parameters = new(arguments);
            ICommand command = new ChangeBugStatusCommand(parameters, repository);
            return command;
        }
    }
}
