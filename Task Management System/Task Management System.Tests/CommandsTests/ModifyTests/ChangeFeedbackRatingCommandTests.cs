﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Commands.Modify;
using Task_Management_System.Core;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Tests.CommandsTests.ModifyTests
{
    [TestClass]
    public class ChangeFeedbackRatingCommandTests
    {
        [TestMethod]
        public void Throw_When_InvalidParameterCount()
        {
            //Arrange
            ICommand command = this.GetCommand("1");
            //Act and Assert
            Assert.ThrowsException<InvalidUserInputException>(() => command.Execute());
        }

        [TestMethod]
        public void Execute_When_OnlyParameterIsHelp()
        {
            //Arrange
            ICommand command = this.GetCommand("help");
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        public void Throw_When_FeedbackIsNotFound()
        {
            //Arrange
            ICommand command = this.GetCommand("123", "6");
            //Act and Assert
            Assert.ThrowsException<EntityNotFoundException>(() => command.Execute());
        }

        [TestMethod]
        [DataRow("4")]
        [DataRow("5")]
        [DataRow("10")]
        public void Execute_When_ValidParameters(string rating)
        {
            //Arrange
            ICommand command = this.GetCommand("valid title", rating);
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        private IRepository GetRepository()
        {
            IRepository result = new Repository();
            result.CreateFeedback("valid title", "valid description", 4, FeedbackStatus.New);
            return result;
        }

        private ICommand GetCommand(params string[] arguments)
        {
            var repository = this.GetRepository();
            List<string> parameters = new(arguments);
            ICommand command = new ChangeFeedbackRatingCommand(parameters, repository);
            return command;
        }
    }
}
