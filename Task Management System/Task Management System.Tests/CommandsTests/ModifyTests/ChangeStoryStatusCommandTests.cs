﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Commands.Modify;
using Task_Management_System.Core;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Tests.CommandsTests.ModifyTests
{
    [TestClass]
    public class ChangeStoryStatusCommandTests
    {
        [TestMethod]
        public void Throw_When_InvalidParameterCount()
        {
            //Arrange
            ICommand command = this.GetCommand("valid title");
            //Act and Assert
            Assert.ThrowsException<InvalidUserInputException>(() => command.Execute());
        }

        [TestMethod]
        public void Execute_When_OnlyParameterIsHelp()
        {
            //Arrange
            ICommand command = this.GetCommand("help");
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        public void Throw_When_TaskIsNotFound()
        {
            //Arrange
            ICommand command = this.GetCommand("123", "notdone");
            //Act and Assert
            Assert.ThrowsException<EntityNotFoundException>(() => command.Execute());
        }

        [TestMethod]
        [DataRow("notdone")]
        [DataRow("inprogress")]
        public void Execute_When_ValidParameters(string status)
        {
            //Arrange
            ICommand command = this.GetCommand("valid title", status);
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        private IRepository GetRepository()
        {
            IRepository result = new Repository();
            result.CreateStory("valid title", "valid description", Priority.High, StoryStatus.NotDone, StorySize.Large);
            return result;
        }

        private ICommand GetCommand(params string[] arguments)
        {
            var repository = this.GetRepository();
            List<string> parameters = new(arguments);
            ICommand command = new ChangeStoryStatusCommand(parameters, repository);
            return command;
        }
    }
}
