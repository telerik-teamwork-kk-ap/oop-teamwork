﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Commands.List;
using Task_Management_System.Core;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Helpers;

namespace Task_Management_System.Tests.CommandsTests.ListTests
{
    [TestClass]
    public class ListStoriesCommandTests
    {
        private Repository GetRepository()
        {
            Repository repository = new();
            repository.CreateMember("Stanimir");
            return repository;
        }

        private ICommand GetCommand(params string[] parameters)
        {
            Repository repository = this.GetRepository();
            IList<string> parametersList = new List<string>(parameters);
            ICommand command = new ListStoriesCommand(parametersList, repository);
            return command;
        }

        [TestMethod]
        public void Return_Info_When_Help_Is_Аsked()
        {
            //Arrange
            var sut = this.GetCommand("help");
            //Act, Assert

            Assert.IsInstanceOfType(sut.Execute(), typeof(string));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Only_One_Parameter_And_Different_From_Help()
        {
            //Arrange
            var sut = this.GetCommand("mandzha");
            //Act, Assert
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Two_Parameters_But_Invalid_Second()
        {
            //Arrange
            var sut = this.GetCommand("view all", "stuff");
            //Act, Assert
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Two_Parameters_But_Invalid_First()
        {
            //Arrange
            var sut = this.GetCommand("viewall", "title");
            //Act, Assert
            sut.Execute();
        }

        [TestMethod]
        public void Return_String_When_All_Two_Parameters_Are_Correct()
        {
            //Arrange
            var sut = this.GetCommand("view all", "title");
            //Act
            var message = sut.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Three_Parameters_But_Invalid_First()
        {
            //Arrange
            var sut = this.GetCommand("smth", "done", "title");
            //Act, Assert
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Three_Parameters_But_Invalid_Second_Case_Status()
        {
            //Arrange
            var sut = this.GetCommand("status", "high", "title");
            //Act, Assert
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Three_Parameters_But_Invalid_Third_Case_Status()
        {
            //Arrange
            var sut = this.GetCommand("status", "notdone", "somethingStupid");
            //Act, Assert
            sut.Execute();
        }

        [TestMethod]
        public void Return_String_When_All_Three_Parameters_Are_Correct_Case_Status()
        {
            //Arrange
            var sut = this.GetCommand("status", "inprogress", "size");
            //Act
            var message = sut.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Three_Parameters_But_Invalid_Second_Case_Assignee()
        {
            //Arrange
            var sut = this.GetCommand("assignee", "Ivanka", "title"); // Ivanka is not a member.
            //Act, Assert
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Three_Parameters_But_Invalid_Third_Case_Assignee()
        {
            //Arrange
            var sut = this.GetCommand("assignee", "Stanimir", "somethingStupid");
            //Act, Assert
            sut.Execute();
        }

        [TestMethod]
        public void Return_String_When_All_Three_Parameters_Are_Correct_Case_Assignee()
        {
            //Arrange
            var sut = this.GetCommand("assignee", "Stanimir", "priority");
            //Act
            var message = sut.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Four_Parameters_But_Invalid_First()
        {
            //Arrange
            var sut = this.GetCommand("smth", "done", "Stanimir", "title");
            //Act, Assert
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Four_Parameters_But_Invalid_Second()
        {
            //Arrange
            var sut = this.GetCommand("status & assignee", "stream", "Stanimir", "title");
            //Act, Assert
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Four_Parameters_But_Invalid_Third()
        {
            //Arrange
            var sut = this.GetCommand("status & assignee", "notdone", "Ivanka", "title");
            //Act, Assert
            sut.Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Four_Parameters_But_Invalid_Forth()
        {
            //Arrange
            var sut = this.GetCommand("status & assignee", "inprogress", "Stanimir", "description");
            //Act, Assert
            sut.Execute();
        }

        [TestMethod]
        public void Return_String_When_All_Four_Parameters_Are_Correct()
        {
            //Arrange
            var sut = this.GetCommand("status & assignee", "done", "Stanimir", "priority");
            //Act
            var message = sut.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Should_Throw_When_Invalid_Number_Of_Parameters()
        {
            //Arrange
            var sut = this.GetCommand("invalid", "status & assignee", "done", "Stanimir", "title");
            //Act, Assert
            sut.Execute();
        }

    }
}