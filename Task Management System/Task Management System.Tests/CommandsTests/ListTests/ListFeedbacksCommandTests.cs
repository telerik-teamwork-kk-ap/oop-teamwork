﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Commands.List;
using Task_Management_System.Core;
using Task_Management_System.Helpers;

namespace Task_Management_System.Tests.CommandsTests.ListTests
{
    [TestClass]
    public class ListFeedbacksCommandTests
    {
        [TestClass]
        public class ListStoriesCommandTests
        {
            private Repository GetRepository()
            {
                Repository repository = new();
                return repository;
            }

            private ICommand GetCommand(params string[] parameters)
            {
                Repository repository = this.GetRepository();
                IList<string> parametersList = new List<string>(parameters);
                ICommand command = new ListFeedbacksCommand(parametersList, repository);
                return command;
            }

            [TestMethod]
            public void Return_Info_When_Help_Is_Аsked()
            {
                //Arrange
                var sut = this.GetCommand("help");
                //Act, Assert

                Assert.IsInstanceOfType(sut.Execute(), typeof(string));
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidUserInputException))]
            public void Should_Throw_When_Only_One_Parameter_And_Different_From_Help()
            {
                //Arrange
                var sut = this.GetCommand("mandzha");
                //Act, Assert
                sut.Execute();
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidUserInputException))]
            public void Should_Throw_When_Two_Parameters_But_Invalid_Second()
            {
                //Arrange
                var sut = this.GetCommand("view all", "stuff");
                //Act, Assert
                sut.Execute();
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidUserInputException))]
            public void Should_Throw_When_Two_Parameters_But_Invalid_First()
            {
                //Arrange
                var sut = this.GetCommand("viewall", "title");
                //Act, Assert
                sut.Execute();
            }


            [TestMethod]
            public void Return_String_When_All_Two_Parameters_Are_Correct()
            {
                //Arrange
                var sut = this.GetCommand("view all", "title");
                //Act
                var message = sut.Execute();
                //Assert
                Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidUserInputException))]
            public void Should_Throw_When_Three_Parameters_But_Invalid_First()
            {
                //Arrange
                var sut = this.GetCommand("smth", "new", "title");
                //Act, Assert
                sut.Execute();
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidUserInputException))]
            public void Should_Throw_When_Three_Parameters_But_Invalid_Second_Case_Status()
            {
                //Arrange
                var sut = this.GetCommand("status", "high", "title");
                //Act, Assert
                sut.Execute();
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidUserInputException))]
            public void Should_Throw_When_Three_Parameters_But_Invalid_Third_Case_Status()
            {
                //Arrange
                var sut = this.GetCommand("status", "new", "somethingStupid");
                //Act, Assert
                sut.Execute();
            }

            [TestMethod]
            public void Return_String_When_All_Three_Parameters_Are_Correct_Case_Status()
            {
                //Arrange
                var sut = this.GetCommand("status", "new", "rating");
                //Act
                var message = sut.Execute();
                //Assert
                Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
            }

        }
    }
}
