﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Commands.List;
using Task_Management_System.Core;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Tests.CommandsTests.ListTests
{
    [TestClass]
    public class ListTasksWithAssigneeCommandTests
    {
        [TestMethod]
        public void Throw_When_TooManyParameters()
        {
            //Arrange
            ICommand command = this.GetCommand("kiril st.", "inprogress", "extra");
            //Act and Assert
            Assert.ThrowsException<InvalidUserInputException>(() => command.Execute());
        }

        [TestMethod]
        public void Throw_When_OneParameterIsInvalid()
        {
            //Arrange
            ICommand command = this.GetCommand("stefan k.");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => command.Execute());
        }

        [TestMethod]
        public void Throw_When_FirstParameterIsInvalid()
        {
            //Arrange
            ICommand command = this.GetCommand("stefan k.", "inprogress");
            //Assert
            Assert.ThrowsException<EntityNotFoundException>(() => command.Execute());
        }

        [TestMethod]
        public void Throw_When_SecondParameterIsInvalid()
        {
            //Arrange
            ICommand command = this.GetCommand("kiril st.", "inprogressesss");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => command.Execute());
        }

        [TestMethod]
        public void Execute_When_NoParameters()
        {
            //Arrange
            ICommand command = this.GetCommand();
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        public void Execute_When_OneParameterIsName()
        {
            //Arrange
            ICommand command = this.GetCommand("kiril st.");
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        public void Execute_When_OneParameterIsBugStatus()
        {
            //Arrange
            ICommand command = this.GetCommand("Active");
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        public void Execute_When_OneParameterIsStoryStatus()
        {
            //Arrange
            ICommand command = this.GetCommand("InProgress");
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        public void Execute_When_OneParameterIsFeedbackStatus()
        {
            //Arrange
            ICommand command = this.GetCommand("New");
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        [DataRow("Fixed")]
        [DataRow("yasen st.")]
        public void Execute_When_ValidParameterButNoneFound(string search)
        {
            //Arrange
            ICommand command = this.GetCommand(search);
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        [DataRow("inprogress")]
        [DataRow("active")]
        public void Execute_When_2ValidParameters(string status)
        {
            //Arrange
            ICommand command = this.GetCommand("kiril st.", status);
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        public void Execute_When_2ValidParametersButNoFound()
        {
            //Arrange
            ICommand command = this.GetCommand("yasen st.", "inProgress");
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        public void Execute_When_OnlyParameterIsHelp()
        {
            //Arrange
            ICommand command = this.GetCommand("help");
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        private IRepository GetRepository()
        {
            IRepository result = new Repository();
            var story1 = result.CreateStory("valid title", "valid description", Priority.High, StoryStatus.InProgress, StorySize.Medium);
            result.CreateStory("valid title", "valid description", Priority.High, StoryStatus.InProgress, StorySize.Medium);
            result.CreateStory("valid title", "valid description", Priority.High, StoryStatus.InProgress, StorySize.Medium);
            result.CreateBug("valid title", "valid description", Priority.High, Severity.Major, new List<string> { "open" });
            result.CreateFeedback("valid title", "valid description", 4, FeedbackStatus.New);
            var assignee = result.CreateMember("kiril st.");
            var freeAssignee = result.CreateMember("yasen st.");
            assignee.AddTask(story1);
            story1.Assignee = assignee;
            return result;
        }

        private ICommand GetCommand(params string[] arguments)
        {
            var repository = this.GetRepository();
            List<string> parameters = new(arguments);
            ICommand command = new ListTasksWithAssigneeCommand(parameters, repository);
            return command;
        }
    }
}
