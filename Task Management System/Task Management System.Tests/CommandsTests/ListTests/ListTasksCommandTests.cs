﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Commands.List;
using Task_Management_System.Core;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Tests.CommandsTests.ListTests
{
    [TestClass]
    public class ListTasksCommandTests
    {
        [TestMethod]
        public void Throw_When_TooManyParameters()
        {
            //Arrange
            ICommand command = this.GetCommand("key1", "key3");
            //Act and Assert
            Assert.ThrowsException<InvalidUserInputException>(() => command.Execute());
        }

        [TestMethod]
        public void Execute_When_ZeroParameters()
        {
            //Arrange
            ICommand command = this.GetCommand();
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        public void Execute_When_OneParameter()
        {
            //Arrange
            ICommand command = this.GetCommand("key1");
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        public void Execute_When_NoTasksFound()
        {
            //Arrange
            ICommand command = this.GetCommand("key69");
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        public void Execute_When_NoTasks()
        {
            //Arrange
            ICommand command = this.GetCommandForEmptyRepo();
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        [TestMethod]
        public void Execute_When_OnlyParameterIsHelp()
        {
            //Arrange
            ICommand command = this.GetCommand("help");
            //Act
            string message = command.Execute();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(message));
        }

        private IRepository GetRepository()
        {
            IRepository result = new Repository();
            result.CreateBug("Valid title1", "Valid Description key1", Priority.High, Severity.Major, new List<string> { "open", "use", "close" });
            result.CreateBug("Valid title2", "Valid Description key1", Priority.High, Severity.Major, new List<string> { "open", "use", "close" });
            result.CreateBug("Valid title3", "Valid Description key2", Priority.High, Severity.Major, new List<string> { "open", "use", "close" });
            result.CreateBug("Valid title4", "Valid Description key3", Priority.High, Severity.Major, new List<string> { "open", "use", "close" });
            return result;
        }

        private IRepository GetEmptyRepository()
        {
            IRepository result = new Repository();
            return result;
        }

        private ICommand GetCommandForEmptyRepo(params string[] arguments)
        {
            var repository = this.GetEmptyRepository();
            List<string> parameters = new(arguments);
            ICommand command = new ListTasksCommand(parameters, repository);
            return command;
        }

        private ICommand GetCommand(params string[] arguments)
        {
            var repository = this.GetRepository();
            List<string> parameters = new(arguments);
            ICommand command = new ListTasksCommand(parameters, repository);
            return command;
        }
    }
}
