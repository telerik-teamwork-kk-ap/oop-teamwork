﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Task_Management_System.Models;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Tests.Models
{
    public class TaskDummy : Task, IAssignableTask
    {
        public TaskDummy(int id, string title, string description) : base(id, title, description)
        {
        }

        public IMember Assignee { get; set; }
    }
}
