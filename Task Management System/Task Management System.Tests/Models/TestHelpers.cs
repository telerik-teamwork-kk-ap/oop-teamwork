﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models;
using Task_Management_System.Models.Enums;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.Models
{
    internal static class TestHelpers
    {
        public const string ChangeObjectProperty = "{0} of task with ID {1} was changed from {2} to {3}.";
        
        public static Bug GetBug()
        {
            return new Bug(1, "Bug’s title ", "This is one pretty serious bug. It needs to be managed.", Priority.High, Severity.Critical,
                new List<string>() { "First you have to open the program.", "Then type “I search for a bug”.", "And then if you are lucky we hope you will find one." });
        }

        public static Feedback GetFeedback()
        {
            return new Feedback(2, "Feedback's title", "Your work on this project is great. Keep doing the same way. ;)", 
                10, FeedbackStatus.Done);
        }

        public static Story GetStory()
        {
            return new Story(3, "A Beautiful Day", " I hope this morning the weather outside is warm and sunny. If it is so I will go for a walk. But if it is not, I will stay at home.", 
                Priority.Medium, StoryStatus.NotDone, StorySize.Large);
        }

        public static TaskDummy GetTaskDummy()
        {
            return new TaskDummy(4, "Task's title", "This is the task's description.");
        }

    }
}
