﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Tests.Models
{
    public class FakeTask : IAssignableTask
    {
        public int Id => throw new NotImplementedException();

        public string Title { get; set; }

        public string Description => throw new NotImplementedException();

        public IReadOnlyList<IComment> Comments => throw new NotImplementedException();

        public IReadOnlyList<IEvent> EventHistory => throw new NotImplementedException();

        public IMember Assignee { get; set; }

        public void AddCommentToTask(Comment comment)
        {
            throw new NotImplementedException();
        }

        public string HistoryToString()
        {
            throw new NotImplementedException();
        }
    }
}
