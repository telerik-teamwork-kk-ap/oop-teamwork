﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Tests.Models
{
    public class FakeMember : IMember
    {
        public IReadOnlyList<IAssignableTask> Tasks => throw new NotImplementedException();

        public string Name { get; set; }

        public IReadOnlyList<IEvent> EventHistory => throw new NotImplementedException();

        public void AddTask(IAssignableTask taskToAdd)
        {
            throw new NotImplementedException();
        }

        public IAssignableTask FindTaskInMemberTasks(string taskTitle)
        {
            throw new NotImplementedException();
        }

        public string HistoryToString()
        {
            throw new NotImplementedException();
        }

        public void RemoveTask(IAssignableTask taskToRemove)
        {
            throw new NotImplementedException();
        }
    }
}
