﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.BoardTests
{
    [TestClass]
    public class AddOrRemoveTask_Should
    {
        private const int NameMinLength = 5;
        private const int NameMaxLength = 10;


        [TestMethod]
        public void AddATask()
        {
            //Arrange
            string name = new('a', NameMinLength);
            IBoard board = new Board(name);
            ITask task = new FakeTask();
            //Act
            board.AddTask(task);
            //Assert
            Assert.AreEqual(board.Tasks.Count, 1);
        }

        [TestMethod]
        public void RemoveATask()
        {
            //Arrange
            string name = new('a', NameMinLength);
            IBoard board = new Board(name);
            ITask task = new FakeTask();
            board.AddTask(task);
            //Act
            board.RemoveTask(task);
            //Assert
            Assert.AreEqual(board.Tasks.Count, 0);
        }

        [TestMethod]
        public void AddAnEvent_When_TaskIsAdded()
        {
            //Arrange
            string name = new('a', NameMinLength);
            IBoard board = new Board(name);
            ITask task = new FakeTask();
            int historyCount = board.EventHistory.Count;
            //Act
            board.AddTask(task);
            //Assert
            Assert.AreEqual(board.EventHistory.Count, historyCount + 1);
        }

        [TestMethod]
        public void AddAnEvent_When_TaskIsRemoved()
        {
            //Arrange
            string name = new('a', NameMinLength);
            IBoard board = new Board(name);
            ITask task = new FakeTask();
            board.AddTask(task);
            int historyCount = board.EventHistory.Count;
            //Act
            board.RemoveTask(task);
            //Assert
            Assert.AreEqual(board.EventHistory.Count, historyCount + 1);
        }

    }
}
