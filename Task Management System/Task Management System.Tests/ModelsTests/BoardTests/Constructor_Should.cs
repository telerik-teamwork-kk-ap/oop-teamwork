﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Helpers;
using Task_Management_System.Models;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Tests.ModelsTests.BoardTests
{
    [TestClass]
    public class Constructor_Should
    {
        private const int NameMinLength = 5;
        private const int NameMaxLength = 10;


        [TestMethod]
        [DataRow(NameMinLength)]
        [DataRow(NameMaxLength)]
        public void SetCorrectName_When_NameIsValid(int length)
        {
            //Arrange
            string name = new('a', length);
            //Act
            IBoard board = new Board(name);
            //Assert
            Assert.AreEqual(name, board.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        [DataRow(NameMaxLength + 1)]
        [DataRow(NameMinLength - 1)]
        public void Throw_When_InvalidNameLength(int length)
        {
            //Arrange
            string invalidName = new('b', length);
            //Act and Assert
            IBoard board = new Board(invalidName);
        }

        [TestMethod]
        public void InitializeTasksCorrectly_When_NameIsValid()
        {
            //Arrange
            string name = new('a', NameMaxLength);
            //Act
            IBoard board = new Board(name);            
            //Assert
            Assert.IsNotNull(board.Tasks);
        }

        [TestMethod]
        public void InitializeHistoryCorrectly_When_NameIsValid()
        {
            //Arrange
            string name = new('a', NameMaxLength);
            //Act
            IBoard board = new Board(name);           
            //Assert
            Assert.IsNotNull(board.EventHistory);
        }

        [TestMethod]
        public void AddAnEvent_When_NameIsValid()
        {
            //Arrange
            string name = new('a', NameMaxLength);
            //Act
            IBoard board = new Board(name);           
            //Assert
            Assert.AreEqual(board.EventHistory.Count, 1);
        }
    }
}
