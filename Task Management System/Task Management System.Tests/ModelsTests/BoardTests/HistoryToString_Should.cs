﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Tests.ModelsTests.BoardTests
{
    [TestClass]
    public class HistoryToString_Should
    {
        private const int NameMinLength = 5;

        [TestMethod]
        public void ReturnANonEmptyString()
        {
            //Arrange
            string name = new('a', NameMinLength);
            IBoard board = new Board(name);
            //Act
            string history = board.HistoryToString();
            //Assert
            Assert.IsTrue(!string.IsNullOrWhiteSpace(history));
        }
    }
}
