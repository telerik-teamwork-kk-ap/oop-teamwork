﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Helpers;
using Task_Management_System.Models;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Tests.ModelsTests.BoardTests
{
    [TestClass]
    public class Name_Should
    {
        private const int NameMinLength = 5;
        private const int NameMaxLength = 10;

        [TestMethod]
        [DataRow(NameMinLength)]
        [DataRow(NameMaxLength)]
        public void SetCorrectName_When_NameIsValid(int length)
        {
            //Arrange
            string name = new('a', NameMinLength);
            IBoard board = new Board(name);
            //Act
            string expected = new('b', length);
            board.Name = expected;
            //Assert
            Assert.AreEqual(expected, board.Name);
        }

        [TestMethod]
        public void AddEvent_When_NameIsChanged()
        {
            //Arrange
            string name = new('a', NameMinLength);
            IBoard board = new Board(name);
            //Act
            string newName = new('b', NameMinLength);
            int historyCount = board.EventHistory.Count;
            board.Name = newName;
            //Assert
            Assert.AreEqual(1, board.EventHistory.Count - historyCount);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        [DataRow(NameMinLength - 1)]
        [DataRow(NameMaxLength + 1)]
        public void Throw_When_NameIsInvalid(int length)
        {
            //Arrange
            string name = new('a', NameMinLength);
            IBoard board = new Board(name);
            //Act and Assert
            string expected = new('b', length);
            board.Name = expected;
        }
    }
}
