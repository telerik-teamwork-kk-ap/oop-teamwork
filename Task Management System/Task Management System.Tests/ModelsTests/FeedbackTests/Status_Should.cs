﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models;
using Task_Management_System.Models.Enums;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.FeedbackTests
{
    [TestClass]
    public class Status_Should
    {

        [TestMethod]
        public void ChangeValue_When_ValueIsCorrect()
        {
            //Act
            Feedback feddback = TestHelpers.GetFeedback();

            //Arrange
            feddback.Status = FeedbackStatus.Unscheduled;

            //Assert
            Assert.AreEqual(FeedbackStatus.Unscheduled, feddback.Status);
        }

        [TestMethod]
        public void Add_EventLog_When_Value_Changes()
        {
            //Act
            Feedback feedback = TestHelpers.GetFeedback();
            int historyCount = feedback.EventHistory.Count;
            //Arrange
            feedback.Status = FeedbackStatus.New;
            //Assert
            Assert.AreEqual(historyCount + 1, feedback.EventHistory.Count);
        }
    }
}
