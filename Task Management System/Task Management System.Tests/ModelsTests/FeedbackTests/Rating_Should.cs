﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Helpers;
using Task_Management_System.Models;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.FeedbackTests
{
    [TestClass]
    public class Rating_Should
    {
        internal const int RatingMinValue = 1;
        internal const int RatingMaxValue = 10;

        Feedback feedback = TestHelpers.GetFeedback();

        [TestMethod]
        public void ChangeValue_When_ValueIsCorrect()
        {

            //Act
            this.feedback.Rating = 5;

            //Assert
            Assert.AreEqual(5, this.feedback.Rating);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]

        [DataRow(RatingMinValue-1)]
        [DataRow(RatingMaxValue+1)]

        public void Throw_Exception_When_ValueIsOutOfRange(int testValue)
        { 
            //Act
            this.feedback.Rating = testValue;
        }

        [TestMethod]
        public void Add_EventLog_When_Value_Changes()
        {
            //Act
            int historyCount = this.feedback.EventHistory.Count;
            //Arrange
            this.feedback.Rating = 1;
            //Assert
            Assert.AreEqual(historyCount + 1, this.feedback.EventHistory.Count);
        }
    }
}
