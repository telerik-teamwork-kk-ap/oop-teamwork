﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Helpers;
using Task_Management_System.Models;
using Task_Management_System.Models.Enums;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.FeedbackTests
{
    [TestClass]
    public class Constructor_Should
    {
        internal const int RatingMinValue = 1;
        internal const int RatingMaxValue = 10;

        private static int rating = 10;
        private static FeedbackStatus status = FeedbackStatus.Done;
        private static Feedback feedback = new(1,"Feedback's title", "Your work on this project is great. Keep doing the same way. ;)",
                rating, status);

        [TestMethod]

        public void Create_First_EventLog_When_Object_Was_Created()
        {
            //Act & Arrange
            int EventHistoryCount = 1;

            //Assert
            Assert.AreEqual(EventHistoryCount, feedback.EventHistory.Count);
        }

        [TestMethod]
        public void Create_Proper_FeedbackStatus()
        {
            Assert.AreEqual(status, feedback.Status);
        }

        [TestMethod]
        public void Create_Proper_Rating()
        {
            Assert.AreEqual(rating, feedback.Rating);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]

        [DataRow(RatingMinValue - 1)]
        [DataRow(RatingMaxValue + 1)]

        public void Throw_Exception_When_ValueIsOutOfRange(int testValue)
        {
           //Act
           feedback.Rating = testValue;
        }

    }
}
