﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Task_Management_System.Models;
using Task_Management_System.Models.Enums;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.StoryTests
{
    [TestClass]
    public class Constructor_Should
    {
        private static Priority priority = Priority.Medium;
        private static StoryStatus status = StoryStatus.NotDone;
        private static StorySize size = StorySize.Large;

        private static Story story = new(1, "A Beautiful Day", " I hope this morning the weather outside is warm and sunny. If it is so I will go for a walk. But if it is not, I will stay at home.",
                priority, status, size);

        [TestMethod]

        public void Create_First_EventLog_When_Object_Was_Created()
        {
            //Act & Arrange
            int EventHistoryCount = 1;

            //Assert
            Assert.AreEqual(EventHistoryCount, story.EventHistory.Count);
        }

        [TestMethod]
        public void Create_Proper_Priority()
        {
            Assert.AreEqual(priority, story.Priority);
        }

        [TestMethod]
        public void Create_Proper_Size()
        {
            Assert.AreEqual(size, story.Size);
        }

        [TestMethod]
        public void Create_Proper_Status()
        {
            Assert.AreEqual(status, story.Status);
        }

        [TestMethod]
        public void Create_Proper_Assignee()
        {
            Assert.AreEqual(null, story.Assignee);
        }
    }

}
