﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task_Management_System.Models;
using Task_Management_System.Models.Enums;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.StoryTests
{
    [TestClass]
    public class Priority_Should
    {
        [TestMethod]
        public void ChangeValue_When_ValueIsCorrect()
        {
            //Act
            Story story = TestHelpers.GetStory();

            //Arrange
            story.Priority = Priority.Low;

            //Assert
            Assert.AreEqual(Priority.Low, story.Priority);
        }

        [TestMethod]
        public void Add_EventLog_When_Value_Changes()
        {
            //Act
            Story story = TestHelpers.GetStory();
            int historyCount = story.EventHistory.Count;
            //Arrange
            story.Priority = Priority.High;
            //Assert
            Assert.AreEqual(historyCount + 1, story.EventHistory.Count);
        }
    }
}
