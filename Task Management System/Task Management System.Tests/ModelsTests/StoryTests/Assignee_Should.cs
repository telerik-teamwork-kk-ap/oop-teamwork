﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.StoryTests
{
    [TestClass]
    public class Assignee_Should
    {
        Story story = TestHelpers.GetStory();
        Member member = new("Pavlov");
        Member secondMember = new("Spiridon");

        [TestMethod]
        public void ChangeValue_When_ValueIsCorrect()
        {
            //Arrange
            this.story.Assignee = this.member;

            //Assert
            Assert.AreEqual(this.member, this.story.Assignee);
        }

        [TestMethod]
        public void ChangeValue_When_ValueIsCorrect_2()
        {

            //Arrange
            this.story.Assignee = this.member;
            this.story.Assignee = this.secondMember;

            //Assert
            Assert.AreEqual(this.secondMember, this.story.Assignee);
        }

        [TestMethod]
        public void Add_EventLog_When_Value_Changes()
        {
            //Act
            this.member = new Member("Krasimir");
            int historyCount = this.story.EventHistory.Count;

            //Arrange
            this.story.Assignee = this.member;

            //Assert
            Assert.AreEqual(historyCount + 1, this.story.EventHistory.Count);
        }
    }
}
