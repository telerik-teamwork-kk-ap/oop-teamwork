﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models;
using Task_Management_System.Models.Enums;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.StoryTests
{
    [TestClass]
    public class Status_Should
    {

        [TestMethod]
        public void ChangeValue_When_ValueIsCorrect()
        {
            //Act
            Story story = TestHelpers.GetStory();

            //Arrange
            story.Status = StoryStatus.InProgress;

            //Assert
            Assert.AreEqual(StoryStatus.InProgress, story.Status);
        }

        [TestMethod]
        public void Add_EventLog_When_Value_Changes()
        {
            //Act
            Story story = TestHelpers.GetStory();
            int historyCount = story.EventHistory.Count;
            //Arrange
            story.Status = StoryStatus.Done;
            //Assert
            Assert.AreEqual(historyCount + 1, story.EventHistory.Count);
        }
    }
}
