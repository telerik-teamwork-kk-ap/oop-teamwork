﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.TaskTests
{
    [TestClass]
    public class AddCommentToTask_Should
    {
        private static ITask task = TestHelpers.GetTaskDummy();
        private static Member member = new("Marijka");
        private static string message = $"Hello from Marijka!";
        private static Comment comment = new(member, message);

        [TestMethod]
        public void Add_Comment_When_Called_With_Correct_Parameters()
        {
            // Act & Arrange
            var historyCount = task.Comments.Count + 1;
            task.AddCommentToTask(comment);
            // Assert
            Assert.AreEqual(historyCount, task.Comments.Count);
        }

        [TestMethod]
        public void Add_EventLog_When_Called_With_Correct_Parameters()
        {
            // Act & Arrange
            var historyCount = task.EventHistory.Count + 1;
            task.AddCommentToTask(comment);
            // Assert
            Assert.AreEqual(historyCount, task.EventHistory.Count);
        }

    }
}
