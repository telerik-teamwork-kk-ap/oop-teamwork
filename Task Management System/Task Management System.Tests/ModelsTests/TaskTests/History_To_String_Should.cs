﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.TaskTests
{
    [TestClass]
    public class History_To_String_Should
    {
        [TestMethod]
        public void Return_String_Of_All_EventLog_Events()
        {
            var bug = TestHelpers.GetBug();
            bug.Assignee = new Member("Minivan");

            var historyToString = bug.HistoryToString();

            Assert.IsInstanceOfType(historyToString, typeof(string));
        }
    }
}
