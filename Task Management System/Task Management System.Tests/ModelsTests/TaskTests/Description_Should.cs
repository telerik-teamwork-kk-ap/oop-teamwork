﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.TaskTests
{
    [TestClass]
    public class Description_Should
    {
        [TestMethod]
        public void Return_Proper_Description()
        {
            var description = "Description of a task.";
            var task = new TaskDummy(1, "Title of a task", description);
            Assert.AreEqual(description, task.Description);
        }
    }
}
