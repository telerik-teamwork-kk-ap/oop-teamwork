﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task_Management_System.Helpers;
using Task_Management_System.Models;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.TaskTests
{
    [TestClass]
    public class  Constructor_Should
    {
        public const int TitleMinLength = 10;
        public const int TitleMaxLength = 50;
        public const int DescriptionMinLength = 10;
        public const int DescriptionMaxLength = 500;

        private static string title = "Task's title";
        private static string description = "This is the task's description.";

        [TestMethod]
        public void Create_An_Object_When_Parameters_Are_Correct()
        {
            //Arrange
            TaskDummy task2 = new(1, title, description);
            //Assert
            Assert.IsInstanceOfType(task2, typeof(Task));
        }

        [DataRow(TitleMinLength - 1)]
        [DataRow(TitleMaxLength + 1)]
        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Throw_An_Exception_When_Title_Is_OutOfRangе(int testValue)
        {
            //Act & Arrange
            string title2 = new('a', testValue);
            new TaskDummy(2, title2, description);
        }
        [TestMethod]
        [DataRow(DescriptionMinLength - 1)]
        [DataRow(DescriptionMaxLength + 1)]
        
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Throw_An_Exception_When_Description_Is_OutOfRangе(int testValue)
        {
            //Act & Arrange
            string description = new('a', testValue);
            new TaskDummy(1, title, description);
        }



    }
}
