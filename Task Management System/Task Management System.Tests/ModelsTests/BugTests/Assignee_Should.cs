﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.BugTests
{
    [TestClass]
    public class Assignee_Should
    {
        Bug bug = TestHelpers.GetBug();
        Member member = new("Pavlov");
        Member secondMember = new("Spiridon");

        [TestMethod]
        public void ChangeValue_When_ValueIsCorrect()
        {
            //Arrange
            this.bug.Assignee = this.member;

            //Assert
            Assert.AreEqual(this.member, this.bug.Assignee);
        }

        [TestMethod]
        public void ChangeValue_When_ValueIsCorrect_2()
        {

            //Arrange
            this.bug.Assignee = this.member;
            this.bug.Assignee = this.secondMember;

            //Assert
            Assert.AreEqual(this.secondMember, this.bug.Assignee);
        }

        [TestMethod]
        public void Add_EventLog_When_Value_Changes()
        {
            //Act
            int historyCount = this.bug.EventHistory.Count;

            //Arrange
            this.bug.Assignee = this.secondMember;

            //Assert
            Assert.AreEqual(historyCount + 1, this.bug.EventHistory.Count);
        }
    }
}
