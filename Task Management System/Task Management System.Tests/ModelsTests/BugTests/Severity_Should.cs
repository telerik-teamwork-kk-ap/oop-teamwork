﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models;
using Task_Management_System.Models.Enums;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.BugTests
{
    [TestClass]
    public class Severity_Should
    {
        [TestMethod]
        public void ChangeValue_When_ValueIsCorrect()
        {
            //Act
            Bug bug = TestHelpers.GetBug();

            //Arrange
            bug.Severity = Severity.Major;

            //Assert
            Assert.AreEqual(Severity.Major, bug.Severity);
        }

        [TestMethod]
        public void Add_EventLog_When_Value_Changes()
        {
            //Act
            Bug bug = TestHelpers.GetBug();
            int historyCount = bug.EventHistory.Count;
            //Arrange
            bug.Severity = Severity.Minor;
            //Assert
            Assert.AreEqual(historyCount + 1, bug.EventHistory.Count);
        }
    }


}
