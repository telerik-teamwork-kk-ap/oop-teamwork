﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Task_Management_System.Models;
using Task_Management_System.Models.Enums;
using Task_Management_System.Tests.Models;
using System.Collections;

namespace Task_Management_System.Tests.ModelsTests.BugTests
{
    [TestClass]
    public class Constructor_Should
    {
        private static Priority bugPriority = Priority.High;
        private static Severity bugSeverity = Severity.Critical;
        private static List<string> steps = new() { "First you have to open the program.", "Then type “I search for a bug”.", "And then if you are lucky we hope you will find one." };
        private static Bug bug = new(1, "Bug's title", "This is one pretty serious bug. It needs to be managed quickly.",
            bugPriority, bugSeverity, steps);


        [TestMethod]

        public void Create_First_EventLog_When_Object_Is_Created()
        {
            //Act & Arrange
            int EventHistoryCount = 1;

            //Assert
            Assert.AreEqual(EventHistoryCount, bug.EventHistory.Count);
        }

        [TestMethod]
        public void Create_Proper_Priority()
        {
            Assert.AreEqual(bugPriority, bug.Priority);
        }

        [TestMethod]
        public void Create_Proper_Severity()
        {
            Assert.AreEqual(bugSeverity, bug.Severity);
        }

        [TestMethod]
        public void Create_Proper_Status()
        {
            Assert.AreEqual(BugStatus.Active, bug.Status);
        }

        [TestMethod]
        public void Create_Proper_List_StepsToReproduce()
        {
            CollectionAssert.AreEqual(steps, (ICollection)bug.StepsToReproduce);
        }

        [TestMethod]
        public void Create_Proper_Assignee()
        {
            Assert.AreEqual(null, bug.Assignee);
        }

    }
}
