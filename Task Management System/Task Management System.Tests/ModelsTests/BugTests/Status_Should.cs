﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Task_Management_System.Models;
using Task_Management_System.Models.Enums;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.BugTests
{
    [TestClass]
    public class Status_Should
    {

        [TestMethod]
        public void ChangeValue_When_ValueIsCorrect()
        {
            //Act
            Bug bug = TestHelpers.GetBug();

            //Arrange
            bug.Status = BugStatus.Fixed;

            //Assert
            Assert.AreEqual(BugStatus.Fixed, bug.Status);
        }

        [TestMethod]

        public void Add_EventLog_When_Value_Changes()
        {
            //Act
            Bug bug = TestHelpers.GetBug();
            int historyCount = bug.EventHistory.Count;
            //Arrange
            bug.Status = BugStatus.Fixed;
            //Assert
            Assert.AreEqual(historyCount + 1, bug.EventHistory.Count);
        }

    }
}
