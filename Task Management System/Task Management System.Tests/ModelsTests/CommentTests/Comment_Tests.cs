﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task_Management_System.Helpers;
using Task_Management_System.Models;

namespace Task_Management_System.Tests.ModelsTests.CommentTests
{
    [TestClass]
    public class Comment_Tests
    {
        public const int CommentMinLenght = 10;
        public const int CommentMaxLenght = 500;

        private Member author = new("Dostoevski");
        private string message = "I'm still here.";
        [TestMethod]
        public void Constructor_Should_Initialized_Object_When_Parameters_Are_Correct()
        {
            Comment comment = new(this.author, this.message);
            Assert.IsInstanceOfType(comment, typeof(Comment));
        }

        [TestMethod]
        public void Property_Author_Should_Return_Proper_value()
        {
            Comment comment = new(this.author, this.message);
            Assert.AreEqual(this.author, comment.Аuthor);
        }

        [TestMethod]
        public void Property_Message_Should_Return_Proper_value()
        {
            Comment comment = new(this.author, this.message);
            Assert.AreEqual(this.message, comment.Message);
        }

        [TestMethod]
        [DataRow(CommentMinLenght - 1)]
        [DataRow(CommentMaxLenght + 1)]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Property_Message_Should_Throw_Exception_When_Value_OutOfRang(int testValue)
        {
            string text = new('a', testValue);
            Comment comment = new(this.author, text);
        }
    }
}
