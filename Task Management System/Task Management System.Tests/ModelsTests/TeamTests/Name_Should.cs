﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Helpers;
using Task_Management_System.Models;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Tests.ModelsTests.TeamTests
{
    [TestClass]
    public class Name_Should
    {
        private const int NameMinLength = 5;
        private const int NameMaxLength = 15;

        [TestMethod]
        [DataRow(NameMinLength)]
        [DataRow(NameMaxLength)]
        public void SetCorrectName_When_ValidName(int length)
        {
            //Arrange
            string name = new('a', length);
            ITeam team = new Team(name);
            //Act
            string newName = new('b', length);
            team.Name = newName;   
            //Assert
            Assert.AreEqual(newName, team.Name);
        }

        [TestMethod]
        [DataRow(NameMinLength - 1)]
        [DataRow(NameMaxLength + 1)]
        [ExpectedException(typeof(InvalidUserInputException))]
        public void Throw_When_InvalidNameLength(int length)
        {
            //Arrange
            string name = new('a', NameMaxLength);
            ITeam team = new Team(name);
            //Act and Assert
            string newName = new('b', length);
            team.Name = newName;
        }

        [TestMethod]
        public void AddEvent_When_ValidName()
        {
            //Arrange
            string name = new('a', NameMaxLength);
            ITeam team = new Team(name);
            //Act
            string newName = new('b', NameMaxLength);
            int historyCount = team.EventHistory.Count;
            team.Name = newName;
            //Assert
            Assert.AreEqual(historyCount + 1, team.EventHistory.Count);
        }
    }
}
