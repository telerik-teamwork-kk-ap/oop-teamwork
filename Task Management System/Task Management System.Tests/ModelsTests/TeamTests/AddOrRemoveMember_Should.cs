﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.TeamTests
{
    [TestClass]
    public class AddOrRemoveMember_Should
    {

        private const int NameMinLength = 5;
        private const int NameMaxLength = 15;


        [TestMethod]
        public void AddAMember()
        {
            //Arrange
            string name = new('a', NameMaxLength);
            ITeam team = new Team(name);
            IMember member = new FakeMember();
            //Act
            team.AddMember(member);
            //Assert
            Assert.AreEqual(1, team.Members.Count);
        }
    }
}
