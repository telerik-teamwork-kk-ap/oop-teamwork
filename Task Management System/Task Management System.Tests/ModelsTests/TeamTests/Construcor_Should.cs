﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Helpers;
using Task_Management_System.Models;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Tests.ModelsTests.TeamTests
{
    [TestClass]
    public class Construcor_Should
    {
        private const int NameMinLength = 5;
        private const int NameMaxLength = 15;

        [TestMethod]
        [DataRow(NameMinLength)]
        [DataRow(NameMaxLength)]
        public void SetCorrectName_When_NameIsValid(int length)
        {
            //Arrange
            string name = new('a', length);
            //Act
            ITeam team = new Team(name);
            //Assert
            Assert.AreEqual(name, team.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        [DataRow(NameMinLength - 1)]
        [DataRow(NameMaxLength + 1)]
        public void Throw_When_NameIsInvaliLength(int length)
        {
            //Arrange
            string name = new('a', length);
            //Act and Assert
            ITeam team = new Team(name);
        }

        [TestMethod]
        public void InitializeMembers_When_ValidName()
        {
            //Arrange
            string name = new('a', NameMinLength);
            //Act
            ITeam team = new Team(name);
            //Assert
            Assert.IsNotNull(team.Members);
        }

        [TestMethod]
        public void InitializeBoards_When_ValidName()
        {

            //Arrange
            string name = new('a', NameMinLength);
            //Act
            ITeam team = new Team(name);
            //Assert
            Assert.IsNotNull(team.Boards);
        }

        [TestMethod]
        public void InitializeHistory_When_ValidName()
        {
            //Arrange
            string name = new('a', NameMinLength);
            //Act
            ITeam team = new Team(name);
            //Assert
            Assert.IsNotNull(team.EventHistory);
        }
        [TestMethod]
        public void CreateAnEvent_When_ValidName()
        {

            //Arrange
            string name = new('a', NameMinLength);
            //Act
            ITeam team = new Team(name);
            //Assert
            Assert.AreEqual(1, team.EventHistory.Count);
        }
    }
}
