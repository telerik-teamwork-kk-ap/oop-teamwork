﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Models;
using Task_Management_System.Helpers;

namespace Task_Management_System.Tests.ModelsTests.MemberTests
{
    [TestClass]
    public class Constructor_Should
    {
        private const int NameMinLenth = 5;
        private const int NameMaxLenth = 15;


        [TestMethod]
        public void SetCorrectName_When_ValidName()
        {
            //Arrange
            string validName = new('a', NameMinLenth);
            //Act
            IMember member = new Member(validName);
            //Assert
            Assert.AreEqual(validName, member.Name);
            Assert.IsInstanceOfType(member.Tasks, typeof(IReadOnlyList<ITask>));
            Assert.IsInstanceOfType(member.EventHistory, typeof(IReadOnlyList<IEvent>));
        }

        [TestMethod]
        public void InitializeTasks_When_ValidName()
        {
            //Arrange
            string validName = new('a', NameMinLenth);
            //Act
            IMember member = new Member(validName);
            //Assert
            Assert.IsInstanceOfType(member.Tasks, typeof(IReadOnlyList<ITask>));
        }

        [TestMethod]
        public void InitializeHistory_When_ValidName()
        {
            //Arrange
            string validName = new('a', NameMinLenth);
            //Act
            IMember member = new Member(validName);
            //Assert
            Assert.IsInstanceOfType(member.EventHistory, typeof(IReadOnlyList<IEvent>));
        }

        [TestMethod]
        public void CreateAnEvent_When_ValidName()
        {
            //Arrange
            string validName = new('a', NameMinLenth);
            //Act
            IMember member = new Member(validName);
            //Assert
            Assert.AreEqual(member.EventHistory.Count, 1);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        [DataRow(NameMinLenth - 1)]
        [DataRow(NameMaxLenth + 1)]
        public void Throw_When_NameIsInvalidLength(int length)
        {
            //Arrange
            string invalidName = new('a', length);
            //Act and Assert
            IMember member = new Member(invalidName);
        }
    }
}
