﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Helpers;
using Task_Management_System.Models;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.MemberTests
{
    [TestClass]
    public class OtherMethods_Should
    {
        private const int NameMinLenth = 5;
        private const int TitleMinLenth = 10;
        private const int DescrMinLenth = 10;

        [TestMethod]
        public void ReturnNonEmptyString()
        {
            //Arrange
            string validName = new('a', NameMinLenth);
            IMember member = new Member(validName);

            //Assert
            Assert.IsTrue(!string.IsNullOrEmpty(member.ToString()));
            Assert.IsTrue(!string.IsNullOrEmpty(member.HistoryToString()));
        }

        [TestMethod]
        public void FindATaskByTitle()
        {
            //Arrange
            string validName = new('a', NameMinLenth);
            IMember member = new Member(validName);
            string title = new('a', TitleMinLenth);
            string descr = new('a', DescrMinLenth);
            IAssignableTask task = new TaskDummy(1, title, descr);
            member.AddTask(task);
            //Act
            var found = member.FindTaskInMemberTasks(title);
            //Assert
            Assert.IsNotNull(found);
        }

        [TestMethod]
        [ExpectedException(typeof(EntityNotFoundException))]
        public void ThrowWhenNoTaskFound()
        {
            //Arrange
            string validName = new('a', NameMinLenth);
            IMember member = new Member(validName);
            string title = new('a', TitleMinLenth);
            string descr = new('a', DescrMinLenth);
            IAssignableTask task = new TaskDummy(1, title, descr);
            member.AddTask(task);
            //Act and Assert
            title = "alkjsdhfkds";
            member.FindTaskInMemberTasks(title);
        }
    }
}
