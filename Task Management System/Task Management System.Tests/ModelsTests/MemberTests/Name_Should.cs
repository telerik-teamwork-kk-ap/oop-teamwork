﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Helpers;
using Task_Management_System.Models;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Tests.ModelsTests.MemberTests
{
    [TestClass]
    public class Name_Should
    {
        private const int NameMinLenth = 5;
        private const int NameMaxLenth = 15;
        private IMember member;
        private int historyCount;

        [TestInitialize]
        public void Init()
        {
            //Arrange
            string validName = new('a', NameMinLenth);
            this.member = new Member(validName);
            this.historyCount = this.member.EventHistory.Count;
        }

        [TestMethod]
        public void SetCoreectName_When_NameIsValid()
        {
            //Arrange
            string newName = new('b', NameMaxLenth);
            //Act
            this.member.Name = newName;
            //Assert
            Assert.AreEqual(newName, this.member.Name);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidUserInputException))]
        [DataRow(NameMinLenth - 1)]
        [DataRow(NameMaxLenth + 1)]
        public void Throw_When_InvalidName(int length)
        {
            //Arrange
            string newName = new('b', length);
            //Act and Assert
            this.member.Name = newName;
        }

        [TestMethod]
        public void AddEvent_When_NameIsValid()
        {
            //Arrange
            string newName = new('b', NameMaxLenth);
            //Act
            this.member.Name = newName;
            //Assert
            Assert.AreEqual(this.member.EventHistory.Count, this.historyCount + 1);
        }
    }
}
