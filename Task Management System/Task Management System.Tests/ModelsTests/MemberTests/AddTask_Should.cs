﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Tests.Models;

namespace Task_Management_System.Tests.ModelsTests.MemberTests
{
    [TestClass]
    public class AddTask_Should
    {
        private const int NameMinLenth = 5;
        private const int TitleMinLenth = 10;
        private const int DescrMinLenth = 10;      

        [TestMethod]
        public void AddATask()
        {
            //Arrange
            string validName = new('a', NameMinLenth);
            IMember member = new Member(validName);
            IAssignableTask task = new FakeTask();
            //Act
            member.AddTask(task);
            //Asser
            Assert.AreEqual(1, member.Tasks.Count);
        }

    }
}
