﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Models
{
    public class Team : ITeam
    {
        private const int NameMinLength = 5;
        private const int NameMaxLength = 15;

        private const string BoardAddedEventMessage = "Board with name {0} was created.";
        private const string MemberJoinedEventMessage = "Member with name {0} joined.";
        private const string NameChangedEventMessage = "Name was changed from {0} to {1}";
        private const string ConstructorEventMessage = "Team with name {0} was created";

        private IList<IMember> members;
        private IList<IBoard> boards;
        private IList<IEvent> eventHistory;
        private string name;

        public Team(string name)
        {
            this.Name = name;
            this.eventHistory = new List<IEvent>();
            this.boards = new List<IBoard>();
            this.members = new List<IMember>();

            string message = string.Format(ConstructorEventMessage, this.Name);
            this.AddEvent(message);
        }

        public IReadOnlyList<IMember> Members => new List<IMember>(this.members);

        public IReadOnlyList<IBoard> Boards => new List<IBoard>(this.boards);

        public string Name
        {
            get => this.name;
            set
            {
                ValidationMethods.ValidateStringLength(value.Length, NameMinLength, NameMaxLength, "Team name");
                ValidationMethods.ValidateNotNull(value, "Team name");
                if (this.name != null)
                {
                    string message = string.Format(NameChangedEventMessage, this.name, value);
                    this.AddEvent(message);
                }
                this.name = value;
            }
        }

        public IReadOnlyList<IEvent> EventHistory => new List<IEvent>(this.eventHistory);

        private void AddEvent(string message)
        {
            this.eventHistory.Add(new Event(message));
        }

        public void AddBoard(IBoard boardToAdd)
        {
            string message = string.Format(BoardAddedEventMessage, boardToAdd.Name);
            this.boards.Add(boardToAdd);
            this.AddEvent(message);
        }

        public void AddMember(IMember memberToAdd)
        {
            string message = string.Format(MemberJoinedEventMessage, memberToAdd.Name);
            this.members.Add(memberToAdd);
            this.AddEvent(message);
        }

        public string HistoryToString()
        {
            var sb = new StringBuilder();
            foreach (var item in this.eventHistory)
            {
                sb.AppendLine(item.ToString());
            }
            return sb.ToString().Trim();
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"Team {this.Name}");
            sb.AppendLine("Team members:");
            foreach (var member in this.members)
            {
                sb.AppendLine($" - {member.Name}");
            }
            if (this.Members.Count == 0)
            {
                sb.AppendLine(" - No Members -");
            }
            sb.AppendLine();
            sb.AppendLine($"Team boards:");
            if (this.Boards.Count == 0)
            {
                sb.AppendLine(" - No Boards -");
            }
            foreach (var board in this.boards)
            {
                sb.AppendLine($" - {board.Name}");
            }
            return sb.ToString().Trim();
        }

        public string MembersToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"{this.Name}'s members:");
            if (this.Members.Count == 0)
            {
                sb.AppendLine(" - No Members -");
            }
            foreach (var member in this.members)
            {
                sb.AppendLine($" - {member.Name}");
            }
            return sb.ToString().Trim();
        }

        public string BoardsToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"{this.Name}'s boards:");
            if (this.Boards.Count == 0)
            {
                sb.AppendLine(" - No Boards -");
            }
            foreach (var board in this.boards)
            {
                sb.AppendLine($" - {board.Name}");
            }
            return sb.ToString().Trim();
        }
    }
}
