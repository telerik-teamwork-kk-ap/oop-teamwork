﻿using Task_Management_System.Helpers;
using System.Collections.Generic;
using System.Text;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Models
{
    public class Bug : Task, IBug
    {
        private IList<string> stepsToReproduce;

        private Severity severity;
        private BugStatus status;
        private Priority priority;
        private IMember assignee;

        public Bug(int id, string title, string description, Priority priority, Severity severity, List<string> stepsToReproduce)
            : base(id, title, description)
        {
            this.priority = priority;
            this.severity = severity;
            this.status = BugStatus.Active;
            this.StepsToReproduce = stepsToReproduce;
            this.assignee = null;
            this.AddEventLog(string.Format(Constants.ObjectCreated, "Bug", this.Title));
        }

        public Severity Severity
        {
            get
            {
                return this.severity;
            }
            set
            {
                this.AddEventLog(string.Format(Constants.ChangeObjectProperty, "Severity", this.Id, this.severity, value));
                this.severity = value;
            }
        }
        public BugStatus Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.AddEventLog(string.Format(Constants.ChangeObjectProperty, "Status", this.Id, this.status, value));
                this.status = value;
            }
        }
        public Priority Priority
        {
            get
            {
                return this.priority;
            }
            set
            {
                this.AddEventLog(string.Format(Constants.ChangeObjectProperty, "Priority", this.Id, this.priority, value));
                this.priority = value;
            }
        }

        public IReadOnlyList<string> StepsToReproduce
        {
            get => new List<string>(this.stepsToReproduce);
            private set
            {
                this.stepsToReproduce = new List<string>(value);
            }
        }

        public IMember Assignee
        {
            get
            {
                return this.assignee;
            }
            set
            {
                if (this.Assignee == null)
                {
                    this.AddEventLog(string.Format(Constants.TaskReceivesAssignee, this.Id, value.Name));
                }
                else if (value != null)
                {
                    this.AddEventLog(string.Format(Constants.ChangeObjectProperty, "Assignee", this.Id, this.assignee.Name, value.Name));
                }
                else if (value == null)
                {
                    this.AddEventLog(string.Format(Constants.TaskWasUnassigned, this.Id));
                }
                this.assignee = value;
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine(base.ToString());
            sb.AppendLine($"Status: {this.Status}");
            sb.AppendLine($"Priority: {this.Priority}");
            sb.AppendLine($"Severity: {this.Severity}");
            sb.AppendLine("StepsToReproduce:");

            foreach (var step in this.StepsToReproduce)
            {
                sb.AppendLine($" - {step.Trim()}");
            }

            if (this.Assignee == null)
            {
                sb.AppendLine("Not assigned");
            }
            else
            {
                sb.AppendLine($"Assignee: {this.Assignee.Name}");
            }

            sb.AppendLine(this.PrintComments());

            return sb.ToString().TrimEnd();
        }
    }
}
