﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Management_System.Models.Contracts
{
    public interface IBoard : IHasName, IHasHistory
    {
        IReadOnlyList<ITask> Tasks { get; }

        void AddTask(ITask task);
        void RemoveTask(ITask task);
    }
}
