﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Management_System.Models.Contracts
{
    public interface IEvent
    {
        string Description { get; }

        DateTime TimeOfEvent { get; }

      //  public string ViewInfo();
    }
}
