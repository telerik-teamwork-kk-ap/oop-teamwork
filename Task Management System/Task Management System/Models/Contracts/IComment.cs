﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Management_System.Models.Contracts
{
    public interface IComment
    {
        IMember Аuthor { get; }
        string Message { get; }

    }
}
