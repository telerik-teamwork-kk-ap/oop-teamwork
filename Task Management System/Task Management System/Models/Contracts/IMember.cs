﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Management_System.Models.Contracts
{
    public interface IMember : IHasName, IHasHistory
    {
        IReadOnlyList<IAssignableTask> Tasks { get; }

        void AddTask(IAssignableTask taskToAdd);

        public IAssignableTask FindTaskInMemberTasks(string taskTitle);

        public void RemoveTask(IAssignableTask taskToRemove);
    }
}
