﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Management_System.Models.Contracts
{
    public interface ITeam : IHasName, IHasHistory
    {
        IReadOnlyList<IMember> Members { get; }

        IReadOnlyList<IBoard> Boards { get; }

        void AddBoard(IBoard boardToAdd);

        void AddMember(IMember memberToAdd);

        string MembersToString();

        string BoardsToString();
    }
}
