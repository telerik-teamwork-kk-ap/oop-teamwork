﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Management_System.Models.Contracts
{
    public interface IHasHistory
    {
        IReadOnlyList<IEvent> EventHistory { get; }

        string HistoryToString();
    }
}
