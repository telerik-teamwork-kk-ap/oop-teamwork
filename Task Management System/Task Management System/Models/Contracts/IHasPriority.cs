﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Models.Contracts
{
    public interface IHasPriority
    {
        Priority Priority { get; set; }
    }
}
