﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Management_System.Models.Contracts
{
    public interface ITask : IHasHistory
    {
        int Id { get; }

        string Title { get; }

        string Description { get; }

        IReadOnlyList<IComment> Comments { get; }


        public void AddCommentToTask(Comment comment);

    }
}
