﻿using System.Text;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Models
{
    public class Story : Task, IStory
    {
        private Priority priority;
        private StorySize size;
        private StoryStatus status;
        private IMember assignee;

        public Story(int id, string title, string description, Priority priority, StoryStatus status, StorySize size)
            : base(id, title, description)
        {
            this.priority = priority;
            this.status = status;
            this.size = size;
            this.assignee = null;
            this.AddEventLog(string.Format(Constants.ObjectCreated, "Story", this.Title));
        }

        public Priority Priority
        {
            get
            {
                return this.priority;
            }
            set
            {
                this.AddEventLog(string.Format(Constants.ChangeObjectProperty, "Priority", this.Id, this.priority, value));
                this.priority = value;
            }
        }
        public StorySize Size
        {
            get
            {
                return this.size;
            }
            set
            {
                this.AddEventLog(string.Format(Constants.ChangeObjectProperty, "Size", this.Id, this.size, value));
                this.size = value;
            }
        }
        public StoryStatus Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.AddEventLog(string.Format(Constants.ChangeObjectProperty, "Status", this.Id, this.status, value));
                this.status = value;
            }
        }
        public IMember Assignee
        {
            get
            {
                return this.assignee;
            }
            set
            {
                if (this.Assignee == null)
                {
                    this.AddEventLog(string.Format(Constants.TaskReceivesAssignee, this.Id, value.Name));
                }
                else if (value != null)
                {
                    this.AddEventLog(string.Format(Constants.ChangeObjectProperty, "Assignee", this.Id, this.assignee.Name, value.Name));
                }
                else if (value == null)
                {
                    this.AddEventLog(string.Format(Constants.TaskWasUnassigned, this.Id));
                }
                this.assignee = value;
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine(base.ToString());
            sb.AppendLine($"Priority: {this.Priority}");
            sb.AppendLine($"Size: {this.Size}");
            sb.AppendLine($"Status: {this.Status}");


            if (this.Assignee == null)
            {
                sb.AppendLine("Not assigned");
            }
            else
            {
                sb.AppendLine($"Assignee: {this.Assignee.Name}");
            }

            sb.AppendLine(this.PrintComments());

            sb.AppendLine();

            return sb.ToString().TrimEnd();

        }

    }
}
