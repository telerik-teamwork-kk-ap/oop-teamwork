﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Models
{
    public class Event : IEvent

    {
        public Event(string description)
        {
            this.Description = description;
            this.TimeOfEvent = DateTime.Now;
        }

        public string Description { get; }

        public DateTime TimeOfEvent { get; }

        public override string ToString()
        {
            return $"[{(this.TimeOfEvent).ToString("dd-MM-yyyy|HH-mm-ss")}] - {this.Description}"; 
        }
    }
}
