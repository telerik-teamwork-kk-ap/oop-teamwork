﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Models
{
    public class Board : IBoard
    {
        public const int NameMinLength = 5;
        public const int NameMaxLength = 10;

        private const string ConstructorEventMessage = "Board with name {0} was created.";
        private const string NameChangedEventMessage = "Name was changhed from {0} to {1}.";
        private const string TaskWasAddedEventMessage = "Task with title '{0}' was added.";
        private const string TaskWasRemovedEventMessage = "Task with title '{0}' was removed.";

        private string name;
        private readonly IList<ITask> tasks;
        private readonly IList<IEvent> eventHistory;

        public Board(string name)
        {
            this.Name = name;
            this.tasks = new List<ITask>();
            this.eventHistory = new List<IEvent>();

            this.AddEvent(string.Format(ConstructorEventMessage, this.Name));
        }

        public IReadOnlyList<ITask> Tasks => new List<ITask>(this.tasks);

        public void AddTask(ITask task)
        {
            this.tasks.Add(task);
            string message = string.Format(TaskWasAddedEventMessage, task.Title);
            this.AddEvent(message);
        }

        public void RemoveTask(ITask task)
        {
            this.tasks.Remove(task);
            string message = string.Format(TaskWasRemovedEventMessage, task.Title);
            this.AddEvent(message);
        }

        public string Name
        {
            get => this.name;
            set
            {
                ValidationMethods.ValidateStringLength(value.Length, NameMinLength, NameMaxLength, "Board name");
                ValidationMethods.ValidateNotNull(value, "Board name");
                if (this.name != null)
                {
                    string message = string.Format(NameChangedEventMessage, this.name, value);
                    this.AddEvent(message);
                }
                this.name = value;
            }
        }
        public IReadOnlyList<IEvent> EventHistory => new List<IEvent>(this.eventHistory);

        private void AddEvent(string message)
        {
            var eventToAdd = new Event(message);
            this.eventHistory.Add(eventToAdd);
        }

        public string HistoryToString()
        {
            var sb = new StringBuilder();
            foreach (var item in this.eventHistory)
            {
                sb.AppendLine(item.ToString());
            }
            return sb.ToString().Trim();
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"Name: {this.Name}");
            sb.AppendLine("Tasks:");
            if (this.Tasks.Count == 0)
            {
                sb.AppendLine(" - No Tasks -");
            }
            foreach (var task in this.tasks)
            {
                sb.AppendLine($" - ID: {task.Id:D4} Title: {task.Title}");
            }
            return sb.ToString().Trim();
        }
    }
}
