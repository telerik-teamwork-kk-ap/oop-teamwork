﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Models
{
    public abstract class Task : ITask
    {
        internal const int TitleMinLength = 10;
        internal const int TitleMaxLength = 50;
        internal const int DescriptionMinLength = 10;
        internal const int DescriptionMaxLength = 500;

        private readonly List<IEvent> eventHistory = new();
        private readonly List<IComment> comments = new();
        protected Task(int id, string title, string description)
        {
            this.Id = id;
            ValidationMethods.ValidateStringLength(title.Length, TitleMinLength, TitleMaxLength, "Title");
            this.Title = title;
            ValidationMethods.ValidateStringLength(description.Length, DescriptionMinLength, DescriptionMaxLength, "Description");
            this.Description = description;
        }
        public int Id { get; }
        public string Title { get; }
        public string Description { get; }
        public IReadOnlyList<IComment> Comments
        {
            get
            {
                return new List<IComment>(this.comments);
            }
        }

        public IReadOnlyList<IEvent> EventHistory
        {
            get
            {
                return new List<IEvent>(this.eventHistory);
            }
        }

        public string HistoryToString()
        {
            return string.Join(Environment.NewLine, this.eventHistory.Select(e => e.ToString())); 
        }

        protected void AddEventLog(string desc)
        {
            this.eventHistory.Add(new Event(desc));
        }

        public void AddCommentToTask(Comment comment)
        {
            this.comments.Add(comment);
            this.AddEventLog(string.Format(Constants.TaskReceivesComment, this.Id));
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"Task ID: {this.Id}");
            sb.AppendLine($"Title: {this.Title}");
            sb.AppendLine($"Description: {this.Description}");
            return sb.ToString().TrimEnd();
        }

        protected string PrintComments()
        {
            var sb = new StringBuilder();

            int index = 1;

            if (this.comments.Count == 0)
            {
                sb.AppendLine("No comments");
            }
            else
            {
                sb.AppendLine("Comments:");
                foreach (var comment in this.Comments)
                {
                    sb.AppendLine($" {index++} {comment}");
                }
            }

            return sb.ToString();
        }
    }
}
