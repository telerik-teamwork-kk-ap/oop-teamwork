﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Models
{
    public class Member : IMember
    {
        private const int NameMinLength = 5;
        private const int NameMaxLength = 15;

        private const string NameChangedEventMessage = "Name was changed from {0} to {1}.";
        private const string TaskWasAddedEventMessage = "Task with title {0} was added.";
        private const string TaskWasRemovedEventMessage = "Task with title {0} was removed.";
        private const string ConstructorMessage = "Member with name {0} was created.";

        private string name;
        private IList<IEvent> eventHistory;
        private IList<IAssignableTask> tasks;

        public Member(string name)
        {
            this.Name = name;
            this.eventHistory = new List<IEvent>();
            this.tasks = new List<IAssignableTask>();

            string message = string.Format(ConstructorMessage, name);
            this.AddEvent(message);
        }

        public IReadOnlyList<IAssignableTask> Tasks => new List<IAssignableTask>(this.tasks);

        public string Name
        {
            get => this.name;
            set
            {
                ValidationMethods.ValidateStringLength(value.Length, NameMinLength, NameMaxLength, "Member name");
                ValidationMethods.ValidateNotNull(value, "Member name");
                if (this.name != null)
                {
                    string message = string.Format(NameChangedEventMessage, this.name, value);
                    this.AddEvent(message);
                }
                this.name = value;
            }
        }
        public IReadOnlyList<IEvent> EventHistory => new List<IEvent>(this.eventHistory);

        private void AddEvent(string message)
        {
            var eventToAdd = new Event(message);
            this.eventHistory.Add(eventToAdd);
        }

        public string HistoryToString()
        {
            var sb = new StringBuilder();
            foreach (var item in this.eventHistory)
            {
                sb.AppendLine(item.ToString());
            }
            return sb.ToString().Trim();
        }

        public void AddTask(IAssignableTask taskToAdd)
        {
            this.tasks.Add(taskToAdd);
            taskToAdd.Assignee = this;
            string message = string.Format(TaskWasAddedEventMessage, taskToAdd.Title);
            this.AddEvent(message);
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"Name: {this.Name}");
            sb.AppendLine($"Assigned tasks: ");
            if (this.Tasks.Count == 0)
            {
                sb.AppendLine(" - No Tasks -");
            }
            foreach (ITask task in this.tasks)
            {
                sb.AppendLine($" - ID: {task.Id:D4} Title: {task.Title}");
            }
            return sb.ToString().Trim();
        }

        public IAssignableTask FindTaskInMemberTasks(string taskTitle)
        {
            return this.Tasks.FirstOrDefault(t => t.Title == taskTitle) ?? throw new EntityNotFoundException($"This member does not have task '{taskTitle}'.");
        }

        public void RemoveTask(IAssignableTask taskToRemove)
        {
            this.tasks.Remove(taskToRemove);
            string message = string.Format(TaskWasRemovedEventMessage, taskToRemove.Title);
            this.AddEvent(message);
        }
    }
}
