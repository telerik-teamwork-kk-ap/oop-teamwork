﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Management_System.Models.Enums
{
    public enum StoryStatus
    {
        NotDone,
        InProgress,
        Done
    }
}
