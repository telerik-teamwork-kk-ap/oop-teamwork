﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Models
{
    public class Feedback : Task, IFeedback
    {
        public const int RatingMinValue = 1;
        public const int RatingMaxValue = 10;

        private int rating;
        private FeedbackStatus status;

        public Feedback(int id, string title, string description, int rating, FeedbackStatus status)
            : base(id, title, description)
        {
            ValidationMethods.ValidateIntValue(rating, RatingMinValue, RatingMaxValue, "Rating");
            this.rating = rating;
            this.status = status;
            this.AddEventLog(string.Format(Constants.ObjectCreated, "Feedback", this.Title));
        }

        public int Rating
        {
            get => this.rating;

            set
            {
                ValidationMethods.ValidateIntValue(value, RatingMinValue, RatingMaxValue, "Rating");
                this.AddEventLog(string.Format(Constants.ChangeObjectProperty, "Rating", this.Id, this.rating, value));
                this.rating = value;
            }
        }

        public FeedbackStatus Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.AddEventLog(string.Format(Constants.ChangeObjectProperty, "Status", this.Id, this.status, value));
                this.status = value;
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine(base.ToString());
            sb.AppendLine($"Rating: {this.Rating}");
            sb.AppendLine($"Status: {this.Status}");

            sb.AppendLine(this.PrintComments());

            sb.AppendLine();

            return sb.ToString().TrimEnd();

        }
    }
}
