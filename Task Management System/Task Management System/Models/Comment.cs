﻿using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Models
{
    public class Comment : IComment
    {
        public const int CommentMinLenght = 10;
        public const int CommentMaxLenght = 500;
        private string message;

        public Comment(IMember author, string message)
        {
            this.Аuthor = author;
            this.Message = message;
        }

        public IMember Аuthor { get; }

        public string Message
        {
            get
            {
                return this.message;
            }
            private set
            {
                ValidationMethods.ValidateStringLength(value.Length, CommentMinLenght, CommentMaxLenght, "Comment");
                this.message = value;
            }
        }

        public override string ToString()
        {
            return $"'{this.Message}' - {this.Аuthor.Name}";
        }

    }
}
