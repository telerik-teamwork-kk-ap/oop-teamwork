﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;

namespace Task_Management_System.Commands.Create
{
    public class CreateMemberCommand :BaseCommand
    {
        private const string ParameterFormat = "create member / [Member Name]";

        public CreateMemberCommand(IList<string> commandParameters, IRepository repository)
          : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count < 1)
            {              
                throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArguments, "CreateMemberCommand", 1, this.CommandParameters.Count)); ;
            }

            string name = this.CommandParameters[0];

            if (this.Repository.MemberNameExists(name))
            {
                throw new InvalidUserInputException(string.Format(Constants.ObjectExists, "Member", name));
            }

            this.Repository.CreateMember(name);

            return string.Format(Constants.ObjectCreated, "Member", name);
        }

    }
}
