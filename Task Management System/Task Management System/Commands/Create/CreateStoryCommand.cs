﻿using System.Collections.Generic;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Commands.Create
{
    public class CreateStoryCommand : BaseCommand
    {
        private const string ParameterFormat = "create story / [Title] / [Description] / [Priority] / [Status] / [Size]";
        public CreateStoryCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count < 5)
            {
                throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArguments, "CreateStoryCommand", 5, this.CommandParameters.Count));
            }

            string title = this.CommandParameters[0];
            string description = this.CommandParameters[1];
            Priority priority = this.ParseEnumParameter<Priority>(this.CommandParameters[2], "priority");
            StoryStatus status = this.ParseEnumParameter<StoryStatus>(this.CommandParameters[3], "status");
            StorySize size = this.ParseEnumParameter<StorySize>(this.CommandParameters[4], "size");


            if (this.Repository.TaskTitleExists(title))
            {
                throw new InvalidUserInputException(string.Format(Constants.ObjectExists, "Story", title));
            }

            this.Repository.CreateStory(title, description, priority, status, size);

            return string.Format(Constants.ObjectCreated, "Story", title);
        }

    }
}
