﻿using System.Collections.Generic;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;

namespace Task_Management_System.Commands.Create
{
    public class CreateBoardCommand : BaseCommand
    {
        private const string ParameterFormat = "create board / [Team Name] / [Board Name]";
        public CreateBoardCommand(IList<string> commandParameters, IRepository repository)
          : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count < 2)
            {
                throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArguments, "CreateBoardCommand", 2, this.CommandParameters.Count));
            }

            string teamName = this.CommandParameters[0];
            var team = this.Repository.FindTeamByName(teamName);

            if (team == null)
            {
                throw new EntityNotFoundException(string.Format(Constants.ObjectDoesNotExist, "team", teamName));
            }

            string boardName = this.CommandParameters[1];

            if (this.Repository.BoardNameExists(team, boardName))
            {
                throw new InvalidUserInputException(string.Format(Constants.ObjectExists, "board", boardName));
            }

            this.Repository.CreateBoard(team, boardName);

            return string.Format(Constants.ObjectCreated, "Board", boardName);

        }
    }
}
