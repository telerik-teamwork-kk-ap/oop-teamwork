﻿using System.Collections.Generic;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Commands.Create
{
    public class CreateFeedbackCommand : BaseCommand
    {
        private const string ParameterFormat = "create feedback / [Title] / [Description] / [Rating] / [Status]";
        public CreateFeedbackCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count < 4)
            {
                throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArguments, "CreateFeedbackCommand", 4, this.CommandParameters.Count));
            }

            string title = this.CommandParameters[0];
            string description = this.CommandParameters[1];
            int rating = this.ParseIntParameter(this.CommandParameters[2], "rating");
            FeedbackStatus status = this.ParseEnumParameter<FeedbackStatus>(this.CommandParameters[3], "status");

            if (this.Repository.TaskTitleExists(title))
            {
                throw new InvalidUserInputException(string.Format(Constants.ObjectExists, "feedback", title));
            }

            this.Repository.CreateFeedback(title, description, rating, status);

            return string.Format(Constants.ObjectCreated, "Feedback", title);
        }
    }
}
