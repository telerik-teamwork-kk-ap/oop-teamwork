﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;

namespace Task_Management_System.Commands.Create
{
    public class CreateTeamCommand : BaseCommand
    {
        private const string ParameterFormat = "create team / [Team Name]";
        public CreateTeamCommand(IList<string> commandParameters, IRepository repository)
           : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count < 1)
            {
                throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArguments, "CreateTeamCommand", 1, this.CommandParameters.Count));
            }

            string name = this.CommandParameters[0];
            
            if (this.Repository.TeamNameExists(name))
            {
                throw new InvalidUserInputException(string.Format(Constants.ObjectExists, "Team", name));
            }

            this.Repository.CreateTeam(name);

            return string.Format(Constants.ObjectCreated, "Team", name);
        }
    }
}
