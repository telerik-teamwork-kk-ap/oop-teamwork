﻿using System.Collections.Generic;
using System.Linq;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Commands.Create
{
    public class CreateBugCommand : BaseCommand
    {
        private const string ParameterFormat = "create bug / [Title] / [Description] / [Priority] / [Severity] / [List of steps to reproduce]";

        public CreateBugCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count < 5)
            {
                throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArguments, "CreateBugCommand", 5, this.CommandParameters.Count));
            }

            string title = this.CommandParameters[0];
            string description = this.CommandParameters[1];
            Priority priority = this.ParseEnumParameter<Priority>(this.CommandParameters[2], "priority");
            Severity severity = this.ParseEnumParameter<Severity>(this.CommandParameters[3], "severity");
            List<string> stepsToReproduce = this.CommandParameters[4].Split(';').ToList();

            if (this.Repository.TaskTitleExists(title))
            {
                throw new InvalidUserInputException(string.Format(Constants.ObjectExists, "bug", title));
            }

            this.Repository.CreateBug(title, description, priority, severity, stepsToReproduce);

            return string.Format(Constants.ObjectCreated, "Bug", title);
        }
    }
}
