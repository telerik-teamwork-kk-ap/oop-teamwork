﻿using System;
using System.Collections.Generic;
using System.Linq;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;

namespace Task_Management_System.Commands.Add
{
    public class AddBoardToTeamCommand : BaseCommand
    {
        private const string ParameterFormat = "add board to team / [Team Name] / [Board Name]";
        public AddBoardToTeamCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {

        }
        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count < 2)
            {
                throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArguments, "AddBoardToTeamCommand", 2, this.CommandParameters.Count));
            }
            
            string teamName = this.CommandParameters[0];
            string boardName = this.CommandParameters[1];

            var team = this.Repository.FindTeamByName(teamName);

            if (team == null)
            {
                throw new EntityNotFoundException(string.Format(Constants.ObjectDoesNotExist, "team", teamName));
            }

            var board = this.Repository.FindBoardByName(boardName);

            if(board == null)
            {
                throw new EntityNotFoundException(string.Format(Constants.ObjectDoesNotExist, "board", boardName));        
            }

            if (team.Boards.Any(b => b.Name == board.Name))
            {
                return $"Team {team.Name} already has a bord {board.Name}";
            }

            team.AddBoard(board);

            return $"Board '{board.Name}' was added to team '{team.Name}'.";

        }

    }
}
