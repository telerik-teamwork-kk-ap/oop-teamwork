﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models;

namespace Task_Management_System.Commands.Add
{
    public class AddCommentToTaskCommand :BaseCommand
    {
        private const string ParameterFormat = "add comment / [Task Name] / [Author Name] / [Comment]";
        public AddCommentToTaskCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {

        }
        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count < 3)
            {
                throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArguments, "AddCommentToTaskCommand", 3, this.CommandParameters.Count));
            }

            string taskName = this.CommandParameters[0];
            var task = this.Repository.FindTaskByTitle(taskName);
            string memberName = this.CommandParameters[1];
            var member = this.Repository.FindMemberByName(memberName);
            string message = this.CommandParameters[2];

            if (task == null)
            {
                throw new EntityNotFoundException(string.Format(Constants.ObjectDoesNotExist, "task", taskName));
            }

            if (member == null)
            {
                throw new EntityNotFoundException(string.Format(Constants.ObjectDoesNotExist, "member", memberName));
            }

            var comment = new Comment(member, message);

            task.AddCommentToTask(comment);

            return $"A new comment has been added to task '{task.Title}'.";
        }
    }
}
