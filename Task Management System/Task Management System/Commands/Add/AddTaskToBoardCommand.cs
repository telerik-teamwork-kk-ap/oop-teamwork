﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;

namespace Task_Management_System.Commands.Add
{
    public class AddTaskToBoardCommand : BaseCommand
    {
        private const string ParameterFormat = "add task to board / [Task Name] / [Board Name]";
        public AddTaskToBoardCommand(IList<string> commandParameters, IRepository repository)
          : base(commandParameters, repository)
        {

        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count < 2)
            {
                throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArguments, "AddTaskToBoardCommand", 2, this.CommandParameters.Count));
            }

            string taskTitle = this.CommandParameters[0];
            string boardName = this.CommandParameters[1];


            var task = this.Repository.FindTaskByTitle(taskTitle);

            if (task == null)
            {
                throw new EntityNotFoundException(string.Format(Constants.ObjectDoesNotExist, "task", taskTitle));
            }

            var board = this.Repository.FindBoardByName(boardName);

            if (board == null)
            {
                throw new EntityNotFoundException(string.Format(Constants.ObjectDoesNotExist, "board", boardName));
            }

            if (board.Tasks.Any(t => t == task))
            {
                return $"Board '{board.Name}' already has task '{task.Title}'.";
            }

            board.AddTask(task);

            return $"A new task has been added to board '{board.Name}'.";
        }
    }
}
