﻿using System.Collections.Generic;
using System.Linq;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;

namespace Task_Management_System.Commands.Add
{
    public class AddMemberToTeamCommand : BaseCommand
    {
        private const string ParameterFormat = "add member to team / [Team Name] / [Member Name]";

        public AddMemberToTeamCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {

        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count < 2)
            {
                throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArguments, "AddMemberToTeamCommand", 2, this.CommandParameters.Count));
            }

            string teamName = this.CommandParameters[0];
            string memberName = this.CommandParameters[1];

            var team = this.Repository.FindTeamByName(teamName);

            if (team == null)
            {
                throw new EntityNotFoundException(string.Format(Constants.ObjectDoesNotExist, "team", teamName));
            }

            var member = this.Repository.FindMemberByName(memberName);

            if (member == null)
            {
                throw new EntityNotFoundException(string.Format(Constants.ObjectDoesNotExist, "member", memberName));
            }

            if (team.Members.Any(m => m.Name == member.Name))
            {
                return $"Team {team.Name} already has member {member.Name}.";
            }

            team.AddMember(member);

            return $"A new member has been added to team '{team.Name}'.";
        }
    }
}
