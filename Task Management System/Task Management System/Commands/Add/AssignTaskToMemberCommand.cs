﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Commands.Add
{
    public class AssignTaskToMemberCommand : BaseCommand
    {
        private const string ParameterFormat = "assign / [Member Name] / [Task Name]";

        public AssignTaskToMemberCommand(IList<string> commandParameters, IRepository repository)
         : base(commandParameters, repository)
        {

        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count < 2)
            {
                throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArguments, "AssignTaskToMemberCommand", 2, this.CommandParameters.Count));
            }

            string memberName = this.CommandParameters[0];
            string taskTitle = this.CommandParameters[1];
            

            IAssignableTask task = this.Repository.FindBugByTitle(taskTitle);
            task ??= this.Repository.FindStoryByTitle(taskTitle);

            var member = this.Repository.FindMemberByName(memberName);

            if (member == null)
            {
                throw new EntityNotFoundException(string.Format(Constants.ObjectDoesNotExist, "member", memberName));
            }

            if (task == null)
            {
                throw new EntityNotFoundException(string.Format(Constants.ObjectDoesNotExist, "task", taskTitle));
            }

            if (member.Tasks.Any(t => t.Title == taskTitle))
            {
                return $"{member.Name} already has a task {task.Title}.";
            }

            member.AddTask(task);

            return $"A new task has been assigned to {member.Name}.";
        }

    }
}
