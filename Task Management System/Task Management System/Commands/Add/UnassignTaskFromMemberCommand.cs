﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models;

namespace Task_Management_System.Commands.Add
{
    public class UnassignTaskFromMemberCommand : BaseCommand
    {
        private const string ParameterFormat = "unassign / [Member Name] / [Task Name]";
        public UnassignTaskFromMemberCommand(IList<string> commandParameters, IRepository repository)
        : base(commandParameters, repository)
        {

        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count < 2)
            {
                throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArguments, "UnassignTaskFromMemberCommand", 2, this.CommandParameters.Count));
            }
            
            string memberName = this.CommandParameters[0];
            string taskTitle = this.CommandParameters[1];

            var member = this.Repository.FindMemberByName(memberName);

            if (member == null)
            {
                throw new EntityNotFoundException(string.Format(Constants.ObjectDoesNotExist, "member", memberName));
            }
            
            var task = member.FindTaskInMemberTasks(taskTitle);

            member.RemoveTask(task);
            task.Assignee = null;

            return $"Task '{task.Title}' was succsefully unassigned from {member.Name}.";
        }
    }
}
