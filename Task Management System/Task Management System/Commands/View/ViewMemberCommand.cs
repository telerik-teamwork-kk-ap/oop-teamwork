﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Commands.View
{
    public class ViewMemberCommand : BaseCommand
    {
        private const int ExpectedParameterCount = 1;
        private static readonly string InvalidParameterCountMessage = $"Invalid number of parameters. Expected number of parameters {ExpectedParameterCount}.";
        private const string ParameterFormat = "view member / [Member Name]";
        private const string MemberNotFoundMessage = "Member with name {0} wasn't found.";
        public ViewMemberCommand(IList<string> commandParameters, IRepository repository) : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }
            if (this.CommandParameters.Count != ExpectedParameterCount)
            {
                var sb = new StringBuilder();
                sb.AppendLine(InvalidParameterCountMessage);
                throw new InvalidUserInputException(sb.ToString().Trim());
            }
            string memberName = this.CommandParameters[0];
            IMember member = this.Repository.FindMemberByName(memberName);

            if (member == null)
            {
                string message = string.Format(MemberNotFoundMessage, memberName);
                throw new EntityNotFoundException(message);
            }

            return member.ToString();
        }
    }
}
