﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;

namespace Task_Management_System.Commands.View
{
    public class ViewAllMembersCommand : BaseCommand
    {
        public ViewAllMembersCommand(IRepository repository) : base(repository)
        {
        }

        public override string Execute()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Members:");
            foreach (var member in this.Repository.AllMembers)
            {
                sb.AppendLine($" - {member.Name}");
            }
            return sb.ToString().Trim();
        }
    }
}
