﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Commands.View
{
    public class ViewBoardEventHistoryCommand : BaseCommand
    {
        private const int ExpectedParameterCount = 1;
        private static readonly string InvalidParameterCountError = $"Invalid number of parameters. Expected number of parameters {ExpectedParameterCount}.";
        private const string ParameterFormat = "view board history / [Board Name]";
        private const string BoardNotFound = "Board with name {0} was not found.";
        public ViewBoardEventHistoryCommand(IList<string> commandParameters, IRepository repository) : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }
            if (this.CommandParameters.Count != ExpectedParameterCount)
            {
                var sb = new StringBuilder();
                sb.AppendLine(InvalidParameterCountError);
                throw new InvalidUserInputException(sb.ToString().Trim());
            }
            string boardName = this.CommandParameters[0];
            IBoard board = this.Repository.FindBoardByName(boardName);
            if (board == null)
            {
                string message = string.Format(BoardNotFound, boardName);
                throw new EntityNotFoundException(message);
            }
            return board.HistoryToString();
        }
    }
}
