﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Commands.View
{
    public class ViewBoardCommand : BaseCommand
    {
        private const string ParameterFormat = "view board / [Name]";
        private const int ExpectedParameterCount = 1;

        public ViewBoardCommand(IList<string> commandParameters, IRepository repository) : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }
            if (this.CommandParameters.Count != ExpectedParameterCount)
            {
                var error = new StringBuilder();
                error.AppendLine(string.Format(Constants.InvalidNumberOfArguments, ExpectedParameterCount, CommandParameters.Count));
                throw new InvalidUserInputException(error.ToString().Trim());
            }

            IBoard found = this.Repository.FindBoardByName(CommandParameters[0])
                ?? throw new EntityNotFoundException(string.Format(Constants.ObjectDoesNotExist, "board", CommandParameters[0]));

            return found.ToString();
        }
    }
}
