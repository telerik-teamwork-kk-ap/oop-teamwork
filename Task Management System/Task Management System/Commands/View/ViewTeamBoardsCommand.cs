﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Commands.View
{
    public class ViewTeamBoardsCommand : BaseCommand
    {
        private const int ExpectedParameterCount = 1;
        private static readonly string InvalidParameterCountError = $"Invalid number of parameters. Expected number of parameters {ExpectedParameterCount}.";
        private const string ParameterFormat = "view team boards / [Team Name]";
        private const string MemberNotFound = "Member with name {0} was not found.";
        public ViewTeamBoardsCommand(IList<string> commandParameters, IRepository repository) : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }
            if (this.CommandParameters.Count != ExpectedParameterCount)
            {
                var error = new StringBuilder();
                error.AppendLine(InvalidParameterCountError);
                throw new InvalidUserInputException(error.ToString().Trim());
            }
            string teamName = this.CommandParameters[0];
            ITeam team = this.Repository.FindTeamByName(teamName);
            if (team == null)
            {
                string message = string.Format(MemberNotFound, teamName);
                throw new EntityNotFoundException(message);
            }
            return team.BoardsToString();
        }
    }
}
