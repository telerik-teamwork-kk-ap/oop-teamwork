﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;

namespace Task_Management_System.Commands.View
{
    public class ViewSupportedCommandsCommand : BaseCommand
    {
        public ViewSupportedCommandsCommand(IRepository repository) : base(repository)
        {
        }

        public override string Execute()
        {
            string separator = new('-', 20);
            var sb = new StringBuilder();
            sb.AppendLine("All supported commands are:");
            sb.AppendLine(separator);
            sb.AppendLine("add board to team");
            sb.AppendLine("add comment");
            sb.AppendLine("add member to team");
            sb.AppendLine("add task to board");
            sb.AppendLine("assign");
            sb.AppendLine("unassign");
            sb.AppendLine("create board");
            sb.AppendLine("create bug");
            sb.AppendLine("create feedback");
            sb.AppendLine("create member");
            sb.AppendLine("create story");
            sb.AppendLine("create team");
            sb.AppendLine("change bug severity");
            sb.AppendLine("change bug status");
            sb.AppendLine("change feedback rating");
            sb.AppendLine("change feedback status");
            sb.AppendLine("change priority");
            sb.AppendLine("change story status");
            sb.AppendLine("view all members");
            sb.AppendLine("view all teams");
            sb.AppendLine("view board");
            sb.AppendLine("view board history");
            sb.AppendLine("view member");
            sb.AppendLine("view member history");
            sb.AppendLine("view task");
            sb.AppendLine("view task history");
            sb.AppendLine("view team boards");
            sb.AppendLine("view team history");
            sb.AppendLine("view team members");
            sb.AppendLine("list bugs");
            sb.AppendLine("list feedbacks");
            sb.AppendLine("list stories");
            sb.AppendLine("list all tasks");
            sb.AppendLine("list tasks with assignee");
            sb.AppendLine(separator);
            sb.AppendLine("To view comand parameter requirements for a commmand, type \"command name / help\".");
            return sb.ToString().Trim();
        }
    }
}
