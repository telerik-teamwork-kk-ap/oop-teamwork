﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Commands.View
{
    public class ViewTaskHistoryCommand : BaseCommand
    {

        private const int ExpectedParameterCount = 1;
        private static readonly string InvalidParameterCountMessage = $"Invalid number of parameters. Expected number of parameters {ExpectedParameterCount}.";
        private const string ParameterFormat = "view task history / [ID or Title]";
        private const string TaskNotFoundMessage = "Task with {0} {1} wasn't found.";

        public ViewTaskHistoryCommand(IList<string> commandParameters, IRepository repository) : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {

            if (this.IsHelp())
            {
                return ParameterFormat;
            }
            if (this.CommandParameters.Count != ExpectedParameterCount)
            {
                var sb = new StringBuilder();
                sb.AppendLine(InvalidParameterCountMessage);
                throw new InvalidUserInputException(sb.ToString().Trim());
            }

            string searchToken = this.CommandParameters[0];
            ITask task = this.Repository.AllTasks.SingleOrDefault(t => t.Title == searchToken);
            string searchBy = "title";
            if (task == null && int.TryParse(searchToken, out int id))
            {
                searchBy += " or ID";
                task = this.Repository.FindTaskById(id);
            }

            if (task == null)
            {
                string message = string.Format(TaskNotFoundMessage, searchBy, searchToken);
                throw new EntityNotFoundException(message);
            }

            return task.HistoryToString();
        }
    }
}
