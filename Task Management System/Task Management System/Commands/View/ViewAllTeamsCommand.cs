﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;

namespace Task_Management_System.Commands.View
{
    public class ViewAllTeamsCommand : BaseCommand
    {
        public ViewAllTeamsCommand(IRepository repository) : base(repository)
        {
        }

        public override string Execute()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Teams:");
            foreach (var team in this.Repository.Teams)
            {
                sb.AppendLine($" - {team.Name}");
            }
            return sb.ToString().Trim();
        }
    }
}
