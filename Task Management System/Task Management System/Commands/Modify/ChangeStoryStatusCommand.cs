﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Commands.Modify
{
    public class ChangeStoryStatusCommand : BaseCommand
    {
        private const int ExpectedParameterCount = 2;
        private static readonly string InvalidParameterCountError = $"Invalid number of parameters. Expected number of parameters {ExpectedParameterCount}.";
        private const string ParameterFormat = "change story status / [ID or Title] / [New Status]";

        private const string InvalidTitleOrId = "Story with {0} {1} was not found!";

        public ChangeStoryStatusCommand(IList<string> commandParameters, IRepository repository) : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count != ExpectedParameterCount)
            {
                var sb = new StringBuilder();
                sb.AppendLine(InvalidParameterCountError);
                sb.AppendLine(ParameterFormat);
                throw new InvalidUserInputException(sb.ToString().Trim());
            }

            string searchToken = this.CommandParameters[0];
            string searchBy = "title";
            IStory found = this.Repository.FindStoryByTitle(searchToken);
            if (found == null && int.TryParse(searchToken, out int id))
            {
                searchBy += " or ID";
                found = this.Repository.FindStoryById(id);
            }
            if (found == null)
            {
                string message = string.Format(InvalidTitleOrId, searchBy, searchToken);
                throw new EntityNotFoundException(message);
            }

            StoryStatus oldStatus = found.Status;
            StoryStatus newStatus = this.ParseEnumParameter<StoryStatus>(this.CommandParameters[1], "story status");

            if (found.Status == oldStatus)
            {
                return $"Story status already at {found.Status}.";
            }

            found.Status = newStatus;
            return $"Status successfully changed from {oldStatus} to {newStatus}.";
        }
    }
}
