﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Commands.Modify
{
    public class ChangeBugSeverityCommand : BaseCommand
    {
        private const int ExpectedParameterCount = 2;
        private static readonly string InvalidParameterCountError = $"Invalid number of parameters. Expected number of parameters {ExpectedParameterCount}.";
        private const string ParameterFormat = "change bug severity / [ID or Title] / [New Severity]";
        private const string InvalidTitleOrId = "Bug with {0} {1} was not found!";
        public ChangeBugSeverityCommand(IList<string> commandParameters, IRepository repository) : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count != ExpectedParameterCount)
            {
                var sb = new StringBuilder();
                sb.AppendLine(InvalidParameterCountError);
                sb.AppendLine(ParameterFormat);
                throw new InvalidUserInputException(sb.ToString().Trim());
            }

            string searchToken = this.CommandParameters[0];
            string searchBy = "title";
            IBug toUpdate = this.Repository.FindBugByTitle(searchToken);
            if (toUpdate == null && int.TryParse(searchToken, out int id))
            {
                searchBy += " or ID";
                toUpdate = this.Repository.FindBugById(id);
            }
            if (toUpdate == null)
            {
                string message = string.Format(InvalidTitleOrId, searchBy, searchToken);
                throw new EntityNotFoundException(message);
            }

            Severity newSeverity = this.ParseEnumParameter<Severity>(this.CommandParameters[1], "BugSeverity");
            Severity oldSeverity = toUpdate.Severity;

            if (oldSeverity == newSeverity)
            {
                return $"Bug severity already at {oldSeverity}.";
            }

            toUpdate.Severity = newSeverity;
            return $"Bug severity successfully changed from {oldSeverity} to {newSeverity}.";
        }
    }
}
