﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Commands.Modify
{
    public class ChangePriorityCommand : BaseCommand
    {
        private const int ExpectedParameterCount = 2;
        private static readonly string InvalidParameterCountError = $"Invalid number of parameters. Expected number of parameters {ExpectedParameterCount}.";
        private const string ParameterFormat = "change priority / [Title or ID] / [New Priority]";

        private const string TaskNotFound = "Bug or Story with {0} {1} was not found.";

        public ChangePriorityCommand(IList<string> commandParameters, IRepository repository) : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count != ExpectedParameterCount)
            {
                var sb = new StringBuilder();
                sb.AppendLine(InvalidParameterCountError);
                sb.AppendLine(ParameterFormat);
                throw new InvalidUserInputException(sb.ToString().Trim());
            }
            
            IHasPriority found;
            string searchToken = this.CommandParameters[0];
            string searchBy = "title";
            found = this.Repository.FindBugByTitle(searchToken);
            found ??= this.Repository.FindStoryByTitle(searchToken);
            if (found == null && int.TryParse(searchToken, out int id))
            {
                searchBy += " or ID";
                found = this.Repository.FindBugById(id);
                found ??= this.Repository.FindStoryById(id);
            }
            if (found == null)
            {
                string message = string.Format(TaskNotFound, searchBy, this.CommandParameters[0]);
                throw new EntityNotFoundException(message);
            }

            Priority oldPriority = found.Priority;
            Priority newPriority = this.ParseEnumParameter<Priority>(this.CommandParameters[1], "priority");

            if (newPriority == oldPriority)
            {
                return $"Priority already at {oldPriority}.";
            }

            found.Priority = newPriority;
            return $"Priority successfully changed from {oldPriority} to {newPriority}.";
        }
    }
}
