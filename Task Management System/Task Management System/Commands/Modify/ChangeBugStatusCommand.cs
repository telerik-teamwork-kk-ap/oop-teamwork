﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Commands.Modify
{
    public class ChangeBugStatusCommand : BaseCommand
    {
        private const int ExpectedParameterCount = 2;
        private static readonly string InvalidParameterCountError = $"Invalid number of parameters. Expected number of parameters {ExpectedParameterCount}.";
        private const string ParameterFormat = "change bug status / [ID or Title] / [New Status]";

        private const string InvalidTitleOrId = "Bug with {0} {1} was not found!";

        public ChangeBugStatusCommand(IList<string> commandParameters, IRepository repository) : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count != ExpectedParameterCount)
            {
                var sb = new StringBuilder();
                sb.AppendLine(InvalidParameterCountError);
                sb.AppendLine(ParameterFormat);
                throw new InvalidUserInputException(sb.ToString().Trim());
            }

            string searchToken = this.CommandParameters[0];
            string searchBy = "title";
            IBug found = this.Repository.FindBugByTitle(searchToken);
            if (found == null && int.TryParse(searchToken, out int id))
            {
                searchBy += " or ID";
                found = this.Repository.FindBugById(id);
            }
            if (found == null)
            {
                string message = string.Format(InvalidTitleOrId, searchBy, searchToken);
                throw new EntityNotFoundException(message);
            }

            BugStatus oldStatus = found.Status;
            BugStatus newStatus = this.ParseEnumParameter<BugStatus>(this.CommandParameters[1], "bug status");
            if (newStatus == oldStatus)
            {
                return $"Bug status already at {found.Status}.";
            }
            found.Status = newStatus;
            return $"Status successfully changed from {oldStatus} to {newStatus}.";
        }
    }
}
