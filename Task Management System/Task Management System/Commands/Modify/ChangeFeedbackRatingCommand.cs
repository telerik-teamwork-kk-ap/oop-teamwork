﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Commands.Modify
{
    public class ChangeFeedbackRatingCommand : BaseCommand
    {
        private const int ExpectedParameterCount = 2;
        private static readonly string InvalidParameterCountError = $"Invalid number of parameters. Expected number of parameters {ExpectedParameterCount}.";
        private const string ParameterFormat = "change feedback rating / [ID or Title] / [New Rating]";

        private const string InvalidTitleOrId = "Feedback with {0} {1} was not found!";

        public ChangeFeedbackRatingCommand(IList<string> commandParameters, IRepository repository) : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            if (this.CommandParameters.Count != ExpectedParameterCount)
            {
                var sb = new StringBuilder();
                sb.AppendLine(InvalidParameterCountError);
                sb.AppendLine(ParameterFormat);
                throw new InvalidUserInputException(sb.ToString().Trim());
            }

            string searchToken = this.CommandParameters[0];
            string searchBy = "title";
            IFeedback found = this.Repository.FindFeedbackByTitle(searchToken);
            if (found == null && int.TryParse(searchToken, out int id))
            {
                searchBy += " or ID";
                found = this.Repository.FindFeedbackById(id);
            }
            if (found == null)
            {
                string message = string.Format(InvalidTitleOrId, searchBy, searchToken);
                throw new EntityNotFoundException(message);
            }

            int oldRating = found.Rating;
            int newRating = this.ParseIntParameter(this.CommandParameters[1], "new rating");
            if (newRating == found.Rating)
            {
                return $"Rating wasn't changed. Rating already at {newRating}.";
            }

            found.Rating = newRating;
            return $"Rating successfully changed from {oldRating} to {newRating}.";
        }
    }
}
