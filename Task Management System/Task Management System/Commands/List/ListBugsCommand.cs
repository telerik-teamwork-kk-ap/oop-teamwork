﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Commands.List
{
    public class ListBugsCommand : BaseCommand
    {
        private const string ParameterFormat = "list bugs / Filter by: ['status', 'assignee', 'status & assignee' or 'view all'] / \n [StatusName, AssigneeName or leave empty for 'view all'] / Sort by: ['title', 'priority' or 'severity']";
        public ListBugsCommand(IList<string> commandParameters, IRepository repository)
          : base(commandParameters, repository)
        {

        }

        public override string Execute()
        {
            int parameterCount = this.CommandParameters.Count;
            IList<IBug> filteredList;
            IList<IBug> sortedList;
            string sortedBy;

            switch (parameterCount)
            {
                case 1:
                    if (this.CommandParameters[0].ToLower() == "help")
                    {
                        return ParameterFormat;
                    }
                    else
                    {
                        throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArgumentsForMoreComplexedCommands, "ListBugCommand"));
                    }
                case 2:
                    if (this.CommandParameters[0].ToLower() == "view all")
                    {
                        sortedBy = this.CommandParameters[1].ToLower();
                        sortedList = this.BugsSorted(this.Repository.AllBugs.ToList(), sortedBy);
                        return this.PrintList(sortedList);
                    }
                    else
                    {
                        throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArgumentsForMoreComplexedCommands, "ListBugCommand"));
                    }
                case 3:
                    if (this.CommandParameters[0].ToLower() == "status")
                    {
                        BugStatus statusName = this.ParseEnumParameter<BugStatus>(this.CommandParameters[1], "status");
                        filteredList = this.Repository.AllBugs.Where(b => b.Status == statusName).ToList();
                        sortedBy = this.CommandParameters[2].ToLower();
                        sortedList = this.BugsSorted(filteredList, sortedBy);
                        return this.PrintList(sortedList);
                    }
                    else if (this.CommandParameters[0].ToLower() == "assignee")
                    {
                        string memberName = this.CommandParameters[1];
                        var assignee = this.Repository.FindMemberByName(memberName);
                        if (assignee == null)
                        {
                            throw new InvalidUserInputException(string.Format(Constants.ObjectDoesNotExist, "assignee", memberName));
                        }
                        filteredList = this.Repository.AllBugs.Where(b => b.Assignee == assignee).ToList();
                        sortedBy = this.CommandParameters[2].ToLower();
                        sortedList = this.BugsSorted(filteredList, sortedBy);
                        return this.PrintList(sortedList);
                    }
                    else
                    {
                        throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArgumentsForMoreComplexedCommands, "ListBugCommand"));
                    }
                case 4:
                    if (this.CommandParameters[0].ToLower() == "status & assignee")
                    {
                        BugStatus statusName2 = this.ParseEnumParameter<BugStatus>(this.CommandParameters[1], "status");
                        string memberName2 = this.CommandParameters[2];
                        var assignee2 = this.Repository.FindMemberByName(memberName2);
                        if (assignee2 == null)
                        {
                            throw new InvalidUserInputException(string.Format(Constants.ObjectDoesNotExist, "assignee", memberName2));
                        }
                        filteredList = this.Repository.AllBugs.Where(b => b.Status == statusName2 && b.Assignee == assignee2).ToList();
                        sortedBy = this.CommandParameters[3].ToLower();
                        sortedList = this.BugsSorted(filteredList, sortedBy);
                        return this.PrintList(sortedList);
                    }
                    else
                    {
                        throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArgumentsForMoreComplexedCommands, "ListBugCommand"));
                    }
                default:
                    throw new InvalidUserInputException("'List bugs' command operates only with 'help', 'view all', 'status', 'assignee' and 'status & assignee' as first parameter.");
            }
            
        }


        public IList<IBug> BugsSorted(IList<IBug> filteredList, string sortedBy)
        {
            IList<IBug> sortedList = new List<IBug>();


            switch (sortedBy)
            {
                case "title":
                    sortedList = filteredList.OrderBy(b => b.Title).ToList();
                    break;
                case "priority":
                    sortedList = filteredList.OrderBy(b => b.Priority).ToList();
                    break;
                case "severity":
                    sortedList = filteredList.OrderBy(b => b.Severity).ToList();
                    break;

                default: throw new InvalidUserInputException("A bug can be sorted either by 'Title', 'Priority' or 'Severity'.");
            }

            return sortedList;
        }


        public string PrintList(IList<IBug> sortedList)
        {
            if (sortedList.Count == 0)
            {
                return "No such task exists.";
            }
         
            string separator = Environment.NewLine + new string('-', 50) + Environment.NewLine;
            return string.Join(separator, sortedList.Select(b => b.ToString()).ToArray());
        }
    }
}
