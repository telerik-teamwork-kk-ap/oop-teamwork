﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Commands.List
{
    public class ListStoriesCommand : BaseCommand
    {
        private const string ParameterFormat = "list stories / Filter by: ['status', 'assignee', 'status & assignee' or 'view all'] / \n [StatusName, AssigneeName or leave empty for 'view all'] / Sort by: ['title', 'priority' or 'severity']";

        public ListStoriesCommand(IList<string> commandParameters, IRepository repository)
          : base(commandParameters, repository)
        {

        }

        public override string Execute()
        {

            int parameterCount = this.CommandParameters.Count;
            IList<IStory> filteredList;
            IList<IStory> sortedList;
            string sortedBy;

            switch (parameterCount)
            {
                case 1:
                    if (this.CommandParameters[0].ToLower() == "help")
                    {
                        return ParameterFormat;
                    }
                    else
                    {
                        throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArgumentsForMoreComplexedCommands, "ListStoriesCommand"));
                    }
                case 2:
                    if (this.CommandParameters[0].ToLower() == "view all")
                    {
                        sortedBy = this.CommandParameters[1].ToLower();
                        sortedList = this.StoriesSorted(this.Repository.AllStories.ToList(), sortedBy);
                        return this.PrintList(sortedList);
                    }
                    else
                    {
                        throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArgumentsForMoreComplexedCommands, "ListStoriesCommand"));
                    }
                case 3:
                    if (this.CommandParameters[0].ToLower() == "status")
                    {
                        StoryStatus statusName = this.ParseEnumParameter<StoryStatus>(this.CommandParameters[1], "status");
                        filteredList = this.Repository.AllStories.Where(s => s.Status == statusName).ToList();
                        sortedBy = this.CommandParameters[2].ToLower();
                        sortedList = this.StoriesSorted(filteredList, sortedBy);
                        return this.PrintList(sortedList);
                    }
                    else if (this.CommandParameters[0].ToLower() == "assignee")
                    {
                        string memberName = this.CommandParameters[1];
                        var assignee = this.Repository.FindMemberByName(memberName);
                        if (assignee == null)
                        {
                            throw new InvalidUserInputException(string.Format(Constants.ObjectDoesNotExist, "assignee", memberName));
                        }
                        filteredList = this.Repository.AllStories.Where(b => b.Assignee == assignee).ToList();
                        sortedBy = this.CommandParameters[2].ToLower();
                        sortedList = this.StoriesSorted(filteredList, sortedBy);
                        return this.PrintList(sortedList);
                    }
                    else
                    {
                        throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArgumentsForMoreComplexedCommands, "ListStoriesCommand"));
                    }
                case 4:
                    if (this.CommandParameters[0].ToLower() == "status & assignee")
                    {
                        StoryStatus statusName = this.ParseEnumParameter<StoryStatus>(this.CommandParameters[1], "status");
                        string memberName = this.CommandParameters[2];
                        var assignee = this.Repository.FindMemberByName(memberName);
                        if (assignee == null)
                        {
                            throw new InvalidUserInputException(string.Format(Constants.ObjectDoesNotExist, "assignee", memberName));
                        }
                        filteredList = this.Repository.AllStories.Where(b => b.Status == statusName && b.Assignee == assignee).ToList();
                        sortedBy = this.CommandParameters[3].ToLower();
                        sortedList = this.StoriesSorted(filteredList, sortedBy);
                        return this.PrintList(sortedList);
                    }
                    else
                    {
                        throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArgumentsForMoreComplexedCommands, "ListStoriesCommand"));
                    }
                default:
                    throw new InvalidUserInputException("'List stories' command operates only with 'help', 'view all', 'status', 'assignee' and 'status & assignee' as first parameter.");
            }

        }



        public IList<IStory> StoriesSorted(IList<IStory> filteredList, string sortedBy)
        {
            IList<IStory> sortedList = new List<IStory>();


            switch (sortedBy)
            {
                case "title":
                    sortedList = filteredList.OrderBy(s => s.Title).ToList();
                    break;
                case "priority":
                    sortedList = filteredList.OrderBy(s => s.Priority).ToList();
                    break;
                case "size":
                    sortedList = filteredList.OrderBy(s => s.Size).ToList();
                    break;

                default: throw new InvalidUserInputException("A story can be sorted either by 'Title', 'Priority' or 'Size'.");
            }

            return sortedList;
        }


        public string PrintList(IList<IStory> sortedList)
        {
            if (sortedList.Count == 0)
            {
                return "No such task exists.";
            }

            string separator = Environment.NewLine + new string('-', 50) + Environment.NewLine;
            return string.Join(separator, sortedList.Select(b => b.ToString()).ToArray());
        }
    }

}