﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Commands.List
{
    public class ListTasksWithAssigneeCommand : BaseCommand
    {
        private static readonly string ParameterFormat = "list tasks with / <assigneeName> <status>" + Environment.NewLine + "<> - optional parameter";
        private static readonly string InvalidParameterCountError = $"Invalid number of parameters. Expected number of parameters is 0, 1 or 2.";

        private const string InvalidFilterTokenError = "An assignee with name {0} was not found. {0} is not a status.";

        public ListTasksWithAssigneeCommand(IList<string> commandParameters, IRepository repository) : base(commandParameters, repository)
        {

        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

            int count = this.CommandParameters.Count;
            List<ITask> tasks = new();
            string notFoundMessage;

            switch (count)
            {
                case 0:
                    tasks.AddRange(this.Repository.AllTasks.OrderBy(t => t.Title).ToList());
                    notFoundMessage = "There aren't any created tasks at the moment.";
                    break;
                case 1:
                    tasks = this.FilterByOneParameter(out notFoundMessage);
                    break;
                case 2:
                    tasks = this.FilterByTwoParameters(out notFoundMessage);
                    break;
                default:
                    string message = InvalidParameterCountError + Environment.NewLine + ParameterFormat;
                    throw new InvalidUserInputException(message);
            }

            if (tasks == null)
            {
                return notFoundMessage;
            }

            tasks = tasks.OrderBy(t => t.Title).ToList();
            var sb = new StringBuilder();
            sb.AppendLine("Filtered tasks:");
            sb.AppendLine(this.ViewTasks(tasks));
            return sb.ToString().Trim();
        }

        private List<ITask> FilterByOneParameter(out string notFoundMessage)
        {
            string filterBy;
            string filterToken = this.CommandParameters[0];
            filterBy = "assignee";
            List<ITask> result = new();

            IMember assignee = this.Repository.FindMemberByName(filterToken);

            if (assignee != null)
            {
                result.AddRange(assignee.Tasks);
            }
            else if (Enum.TryParse<BugStatus>(filterToken, true, out BugStatus bugStatus))
            {
                filterBy = "status";
                result.AddRange(this.Repository.AllBugs.Where(b => b.Status == bugStatus).ToList());
            }
            else if (Enum.TryParse<StoryStatus>(filterToken, true, out StoryStatus storyStatus))
            {
                filterBy = "status";
                result.AddRange(this.Repository.AllStories.Where(s => s.Status == storyStatus).ToList());
            }
            else if (Enum.TryParse<FeedbackStatus>(filterToken, true, out FeedbackStatus feedbackStatus))
            {
                filterBy = "status";
                result.AddRange(this.Repository.AllFeedbacks.Where(b => b.Status == feedbackStatus).ToList());
            }
            else
            {
                string message = string.Format(InvalidFilterTokenError, filterToken);
                throw new InvalidUserInputException(message);
            }

            if (result.Count > 0)
            {
                notFoundMessage = null;
                return result;
            }
            else
            {
                notFoundMessage = $"There aren't any tasks with {filterBy} '{filterToken}'";
                return null;
            }
        }

        private List<ITask> FilterByTwoParameters(out string notFoundMessage)
        {
            string assigneeName = this.CommandParameters[0];
            string status = this.CommandParameters[1];
            List<ITask> result = new();

            IMember assignee = this.Repository.FindMemberByName(assigneeName);
            if (assignee == null)
            {
                throw new EntityNotFoundException($"Assignee with name '{assigneeName}' doesn't exist.");
            }

            if (Enum.TryParse<BugStatus>(status, true, out BugStatus bugStatus))
            {
                result.AddRange(this.Repository.AllBugs.Where(b => b.Status == bugStatus && b.Assignee == assignee).ToList());
            }
            else if (Enum.TryParse<StoryStatus>(status, true, out StoryStatus storyStatus))
            {
                result.AddRange(this.Repository.AllStories.Where(s => s.Status == storyStatus && s.Assignee == assignee).ToList());
            }
            else
            {
                throw new InvalidUserInputException($"'{status}' is not a valid status.");
            }

            if (result.Count > 0)
            {
                notFoundMessage = null;
                return result;
            }
            else
            {
                notFoundMessage = $"There aren't any added tasks for {assigneeName} with status '{status}'.";
                return null;
            }
        }

        private string ViewTasks(List<ITask> tasks)
        {
            string separator = Environment.NewLine + new string('-', 50) + Environment.NewLine;
            return string.Join(separator, tasks.Select(t => t.ToString()).ToArray());
        }
    }
}
