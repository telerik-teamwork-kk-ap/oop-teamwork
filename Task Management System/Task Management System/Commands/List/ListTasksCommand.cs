﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;

namespace Task_Management_System.Commands.List
{
    public class ListTasksCommand : BaseCommand
    {
        private const string InvalidParameterCountMessage = "Invalid number of parameters. Epected number of parameters is 0 or 1.";
        private static readonly string ParameterFormat = "list all tasks / <search keyword>" + Environment.NewLine + "<> - optional parameter";
        public ListTasksCommand(IList<string> commandParameters, IRepository repository) : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            int parameterCount = this.CommandParameters.Count;

            if (parameterCount == 1 && this.CommandParameters[0].ToLower() == "help")
            {
                return ParameterFormat;
            }

            var sb = new StringBuilder();
            switch (parameterCount)
            {
                case 0:
                    sb.AppendLine("Listing all tasks: ");
                    List<ITask> tasks = this.Repository.AllTasks.OrderBy(t => t.Title).ToList();

                    if (tasks.Count == 0)
                    {
                        sb.AppendLine(" - No Tasks -");
                        break;
                    }

                    foreach (var task in tasks)
                    {
                        sb.AppendLine($" - ID: {task.Id:D4} Title: {task.Title}");
                    }

                    break;
                case 1:
                    string keyword = this.CommandParameters[0];
                    var found = this.Repository.AllTasks.Where(t => t.Title.Contains(keyword) || t.Description.Contains(keyword)).OrderBy(t => t.Title).ToList();

                    sb.AppendLine($"Listing all tasks with keyword {keyword}: ");
                    if (found.Count == 0)
                    {
                        sb.AppendLine($" - No Tasks -");
                        break;
                    }

                    foreach (var task in found)
                    {
                        sb.AppendLine($" - ID: {task.Id:D4} Title: {task.Title}");
                    }

                    break;
                default:
                    string message = InvalidParameterCountMessage + Environment.NewLine + ParameterFormat;
                    throw new InvalidUserInputException(message);
            }

            return sb.ToString().Trim();
        }
    }
}
