﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Commands.List
{
    public class ListFeedbacksCommand : BaseCommand
    {
        private const string ParameterFormat = "list feedbacks / Filter by: ['status' or 'view all'] / [StatusName or leave empty] / Sort by: ['title' or 'rating']";
        public ListFeedbacksCommand(IList<string> commandParameters, IRepository repository)
          : base(commandParameters, repository)
        {

        }

        public override string Execute()
        {
            if (this.IsHelp())
            {
                return ParameterFormat;
            }

           
            if (this.CommandParameters.Count < 2 || this.CommandParameters.Count > 3)
            {
                throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArguments, "ListFeedbackCommand", "3 for 'status' and 2 for 'view all'", this.CommandParameters.Count));                
            }

            if (this.CommandParameters.Count == 2 && this.CommandParameters[0].ToLower() != "view all")
            {
                throw new InvalidUserInputException(string.Format(Constants.InvalidNumberOfArguments, "ListFeedbackCommand", "3 for 'status' and 2 for 'view all'", this.CommandParameters.Count));
            }


            string filterBy = this.CommandParameters[0].ToLower();

            IList<IFeedback> sortedList;

            switch (filterBy)
            {
                case "status":
                    FeedbackStatus statusName = this.ParseEnumParameter<FeedbackStatus>(this.CommandParameters[1], "status");
                    var filteredList = this.Repository.AllFeedbacks.Where(b => b.Status == statusName).ToList();
                    string sortedBy1 = this.CommandParameters[2].ToLower();
                    sortedList = this.FeedbackSorted(filteredList, sortedBy1);
                    return this.PrintList(sortedList);

                case "view all":
                    string sortedBy2 = this.CommandParameters[1].ToLower();
                    sortedList = this.FeedbackSorted(this.Repository.AllFeedbacks.ToList(), sortedBy2);
                    return this.PrintList(sortedList);

                default: throw new InvalidUserInputException("A feedback can be filtered either by 'status' or 'view all'.");

            }

        }


        public IList<IFeedback> FeedbackSorted(IList<IFeedback> filteredList, string sortedBy)
        {
            IList<IFeedback> sortedList = new List<IFeedback>();

            switch (sortedBy)
            {
                case "title":
                    sortedList = filteredList.OrderBy(f => f.Title).ToList();
                    break;
                case "rating":
                    sortedList = filteredList.OrderBy(f => f.Rating).ToList();
                    break;

                default: throw new InvalidUserInputException("A feedback can be sorted either by 'title' or 'rating'.");
            }

            return sortedList;
        }


        public string PrintList(IList<IFeedback> sortedList)
        {
            if (sortedList.Count == 0)
            {
                return "No such task exists.";
            }

            string separator = Environment.NewLine + new string('-', 50) + Environment.NewLine;
            return string.Join(separator, sortedList.Select(b => b.ToString()).ToArray());
        }
    }
}
