﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;

namespace Task_Management_System.Commands
{
    public abstract class BaseCommand : ICommand
    {
        protected BaseCommand(IRepository repository) : this(new List<string>(), repository)
        {
        }

        protected BaseCommand(IList<string> commandParameters, IRepository repository)
        {
            this.Repository = repository;
            this.CommandParameters = commandParameters;
        }

        public IRepository Repository { get; }

        public IList<string> CommandParameters { get; }

        public abstract string Execute();

        protected int ParseIntParameter(string value, string parameterName)
        {
            if (int.TryParse(value, out int result))
            {
                return result;
            }
            throw new InvalidUserInputException($"Invalid value for {parameterName}. Should be an integer number.");
        }

        protected T ParseEnumParameter<T>(string value, string parameterName) where T : struct, Enum
        {
            if (!Enum.TryParse<T>(value, true, out T result))
            {
                throw new InvalidUserInputException($"{value} is not a valid {parameterName}.");
            }
            return result;
        }

        protected bool IsHelp()
        {
            return this.CommandParameters.Count == 1 && this.CommandParameters[0].ToLower() == "help";
        }
    }
}
