﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Management_System.Core.Contracts
{
    public interface IEngine
    {
        void Start();
    }
}
