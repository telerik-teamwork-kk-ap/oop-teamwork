﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Core.Contracts
{
    public interface IRepository
    {
        IReadOnlyList<ITeam> Teams { get; }

        IReadOnlyList<IMember> AllMembers { get; }

        IReadOnlyList<IBoard> AllBoards { get; }

        IReadOnlyList<IBug> AllBugs { get; }

        IReadOnlyList<IFeedback> AllFeedbacks { get; }

        IReadOnlyList<IStory> AllStories { get; }

        IReadOnlyList<ITask> AllTasks { get; }

        IMember CreateMember(string name);

        ITeam CreateTeam(string name);

        IBoard CreateBoard(ITeam team, string name);

        IBug CreateBug(string title, string description, Priority priority, Severity severity, List<string> stepsToReproduce);

        IStory CreateStory(string title, string description, Priority priority, StoryStatus status, StorySize size);

        IFeedback CreateFeedback(string title, string description, int rating, FeedbackStatus status);

        IMember FindMemberByName(string name);

        ITeam FindTeamByName(string name);

        IBoard FindBoardByName(string name);

        ITask FindTaskById(int id);

        ITask FindTaskByTitle(string title);

        IBug FindBugById(int id);

        IBug FindBugByTitle(string title);

        IFeedback FindFeedbackById(int id);

        IFeedback FindFeedbackByTitle(string title);

        IStory FindStoryById(int id);

        IStory FindStoryByTitle(string title);

        bool MemberNameExists(string name);

        bool TeamNameExists(string name);

        bool BoardNameExists(ITeam team, string name);

        bool TaskTitleExists(string title);

    }
}
