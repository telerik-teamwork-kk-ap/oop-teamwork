﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Commands.Contracts;

namespace Task_Management_System.Core.Contracts
{
    public interface ICommandFactory
    {
        ICommand Create(string commandLine); 
    }
}
