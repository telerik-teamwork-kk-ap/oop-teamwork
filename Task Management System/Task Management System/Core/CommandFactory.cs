﻿using System;
using System.Collections.Generic;
using System.Linq;
using Task_Management_System.Commands.Add;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Commands.Create;
using Task_Management_System.Commands.List;
using Task_Management_System.Commands.Modify;
using Task_Management_System.Commands.View;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;

namespace Task_Management_System.Core
{
    public class CommandFactory : ICommandFactory
    {
        private readonly IRepository repository;
        public CommandFactory(IRepository repository)
        {
            this.repository = repository;
        }

        public ICommand Create(string commandLine)
        {
            string[] arguments = commandLine.Split("/", StringSplitOptions.RemoveEmptyEntries);

            string commandName = arguments[0].Trim();
            List<string> commandParameters = arguments.Skip(1).Select(a => a.Trim()).ToList();
            ICommand command = commandName.ToLower() switch
            {
                "help" => new ViewSupportedCommandsCommand(this.repository),
                "add board to team" => new AddBoardToTeamCommand(commandParameters, this.repository),
                "add comment" => new AddCommentToTaskCommand(commandParameters, this.repository),
                "add member to team" => new AddMemberToTeamCommand(commandParameters, this.repository),
                "add task to board" => new AddTaskToBoardCommand(commandParameters, this.repository),
                "assign" => new AssignTaskToMemberCommand(commandParameters, this.repository),
                "unassign" => new UnassignTaskFromMemberCommand(commandParameters, this.repository),
                "create board" => new CreateBoardCommand(commandParameters, this.repository),
                "create bug" => new CreateBugCommand(commandParameters, this.repository),
                "create feedback" => new CreateFeedbackCommand(commandParameters, this.repository),
                "create member" => new CreateMemberCommand(commandParameters, this.repository),
                "create story" => new CreateStoryCommand(commandParameters, this.repository),
                "create team" => new CreateTeamCommand(commandParameters, this.repository),
                "change bug severity" => new ChangeBugSeverityCommand(commandParameters, this.repository),
                "change bug status" => new ChangeBugStatusCommand(commandParameters, this.repository),
                "change feedback rating" => new ChangeFeedbackRatingCommand(commandParameters, this.repository),
                "change feedback status" => new ChangeFeedbackStatusCommand(commandParameters, this.repository),
                "change priority" => new ChangePriorityCommand(commandParameters, this.repository),
                "change story status" => new ChangeStoryStatusCommand(commandParameters, this.repository),
                "view all members" => new ViewAllMembersCommand(this.repository),
                "view all teams" => new ViewAllTeamsCommand(this.repository),
                "view board" => new ViewBoardCommand(commandParameters, this.repository),
                "view board history" => new ViewBoardEventHistoryCommand(commandParameters, this.repository),
                "view member" => new ViewMemberCommand(commandParameters, this.repository),
                "view member history" => new ViewMemberEventHistoryCommand(commandParameters, this.repository),
                "view task" => new ViewTaskCommand(commandParameters, this.repository),
                "view task history" => new ViewTaskHistoryCommand(commandParameters, this.repository),
                "view team boards" => new ViewTeamBoardsCommand(commandParameters, this.repository),
                "view team history" => new ViewTeamEventHistoryCommand(commandParameters, this.repository),
                "view team members" => new ViewTeamMembersCommand(commandParameters, this.repository),
                "list bugs" => new ListBugsCommand(commandParameters, this.repository),
                "list feedbacks" => new ListFeedbacksCommand(commandParameters, this.repository),
                "list stories" => new ListStoriesCommand(commandParameters, this.repository),
                "list all tasks" => new ListTasksCommand(commandParameters, this.repository),
                "list tasks with" => new ListTasksWithAssigneeCommand(commandParameters, this.repository),


                _ => throw new InvalidUserInputException($"Command with name: {commandName} doesn't exist!"),
            };
            return command;
        }
    }
}
