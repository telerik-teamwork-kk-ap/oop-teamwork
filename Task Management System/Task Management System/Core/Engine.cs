﻿using System;
using System.Text;
using Task_Management_System.Commands.Contracts;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;

namespace Task_Management_System.Core
{
    public class Engine : IEngine
    {
        private const string TerminationCommand = "end";
        private const string EmptyCommandError = "Command cannot be empty.";
        private const string EnterCommand = "Please enter command name and arguments separeted by / .";
        private const string ChooseList = "To view all available commands type 'help'.";

        private readonly ICommandFactory commandFactory;
        public Engine(ICommandFactory commandFactory)
        {
            this.commandFactory = commandFactory;
        }
        public void Start()
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.Clear();
            while (true)
            {
                try
                {
                    Console.ForegroundColor = ConsoleColor.DarkMagenta;
                    Console.WriteLine();
                    Console.WriteLine(EnterCommand + " " + ChooseList);
                    Console.WriteLine();

                    string input = Console.ReadLine().Trim();

                    if (string.IsNullOrWhiteSpace(input))
                    {
                        Console.WriteLine(EmptyCommandError);
                        continue;
                    }


                    if (input.Equals(TerminationCommand, StringComparison.InvariantCultureIgnoreCase))
                    {
                        break;
                    }

                    Console.WriteLine();

                    ICommand command = this.commandFactory.Create(input);
                    string result = command.Execute();
                    int dashesCount = Math.Min(Console.WindowWidth - 1, result.Length);

                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine(new string('-', dashesCount));
                    Console.WriteLine(result);
                    Console.WriteLine(new string('-', dashesCount));
                }
                catch (InvalidUserInputException ex)
                {
                    int dashesCount = Math.Min(Console.WindowWidth - 1, ex.Message.Length);
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(new string('-', dashesCount));
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(new string('-', dashesCount));
                }
                catch (EntityNotFoundException ex)
                {
                    int dashesCount = Math.Min(Console.WindowWidth - 1, ex.Message.Length);
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(new string('-', dashesCount));
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(new string('-', dashesCount));
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    if (!string.IsNullOrEmpty(ex.Message))
                    {
                        int dashesCount = Math.Min(Console.WindowWidth - 1, ex.Message.Length);
                        Console.WriteLine(new string('-', dashesCount));
                        Console.WriteLine(ex.Message);
                        Console.WriteLine(new string('-', dashesCount));
                    }
                    else
                    {
                        int dashesCount = Math.Min(Console.WindowWidth - 1, ex.ToString().Length);
                        Console.WriteLine(new string('-', ex.ToString().Length));
                        Console.WriteLine(ex);
                        Console.WriteLine(new string('-', ex.ToString().Length));
                    }
                }

            }


        }

    }
}
