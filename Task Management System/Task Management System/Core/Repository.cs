﻿using System.Collections.Generic;
using System.Linq;
using Task_Management_System.Core.Contracts;
using Task_Management_System.Helpers;
using Task_Management_System.Models;
using Task_Management_System.Models.Contracts;
using Task_Management_System.Models.Enums;

namespace Task_Management_System.Core
{
    public class Repository : IRepository
    {
        private readonly IList<ITeam> teams;
        private readonly IList<IMember> allMembers;

        private readonly IList<IBug> allBugs;
        private readonly IList<IStory> allStories;
        private readonly IList<IFeedback> allFeedbacks;

        private int nextId;

        public Repository()
        {
            this.nextId = 1;
            this.teams = new List<ITeam>();
            this.allMembers = new List<IMember>();

            this.allBugs = new List<IBug>();
            this.allStories = new List<IStory>();
            this.allFeedbacks = new List<IFeedback>();

        }

        public IReadOnlyList<ITeam> Teams => new List<ITeam>(this.teams);

        public IReadOnlyList<IMember> AllMembers => new List<IMember>(this.allMembers);

        public IReadOnlyList<IBoard> AllBoards
        {
            get
            {
                List<IBoard> allBoards = new();

                foreach (var team in this.Teams)
                {
                    allBoards.AddRange(team.Boards);
                }

                return allBoards;
            }

        }

        public IReadOnlyList<IBug> AllBugs => new List<IBug>(this.allBugs);

        public IReadOnlyList<IFeedback> AllFeedbacks => new List<IFeedback>(this.allFeedbacks);

        public IReadOnlyList<IStory> AllStories => new List<IStory>(this.allStories);

        public IReadOnlyList<ITask> AllTasks
        {
            get
            {
                List<ITask> tasks = new();

                tasks.AddRange(this.allBugs);
                tasks.AddRange(this.allFeedbacks);
                tasks.AddRange(this.allStories);

                return tasks;
            }
        }


        public IBoard CreateBoard(ITeam team, string name)
        {
            var board = new Board(name);
            team.AddBoard(board);
            return board;
        }

        public IBug CreateBug(string title, string description, Priority priority, Severity severity, List<string> stepsToReproduce)
        {
            var bug = new Bug(nextId, title, description, priority, severity, stepsToReproduce);  ;

            this.allBugs.Add(bug);

            nextId++;

            return bug;
        }

        public IFeedback CreateFeedback(string title, string description, int rating, FeedbackStatus status)
        {
            var feedback = new Feedback(nextId, title, description, rating, status);

            this.allFeedbacks.Add(feedback);

            nextId++;

            return feedback;
        }

        public IMember CreateMember(string name)
        {
            var member = new Member(name);
            this.allMembers.Add(member);
            return member;
        }

        public IStory CreateStory(string title, string description, Priority priority, StoryStatus status, StorySize size)
        {
            var story = new Story(nextId, title, description, priority, status, size);

            this.allStories.Add(story);
            nextId++;

            return story;
        }

        public ITeam CreateTeam(string name)
        {
            var team = new Team(name);

            this.teams.Add(team);

            return team;
        }

        public IBoard FindBoardByName(string name)
        {
            return this.AllBoards.SingleOrDefault(b => b.Name == name);
        }

        public IMember FindMemberByName(string name)
        {
            return this.allMembers.SingleOrDefault(m => m.Name == name);
        }

        public ITask FindTaskById(int id)
        {
            return this.AllTasks.SingleOrDefault(t => t.Id == id);
        }

        public ITeam FindTeamByName(string name)
        {
            return this.Teams.SingleOrDefault(t => t.Name == name);
        }

        public bool MemberNameExists(string name)
        {
            return this.AllMembers.Any(m => m.Name == name);
        }

        public bool TeamNameExists(string name)
        {
            return this.Teams.Any(t => t.Name == name);
        }

        public bool BoardNameExists(ITeam team, string name)
        {
            return team.Boards.Any(b => b.Name == name);
        }

        public bool TaskTitleExists(string title)
        {
            return this.AllTasks.Any(t => t.Title == title);
        }

        public ITask FindTaskByTitle(string title)
        {
            return this.AllTasks.SingleOrDefault(t => t.Title == title);
        }

        public IBug FindBugById(int id)
        {
            return this.AllBugs.SingleOrDefault(b => b.Id == id);
        }

        public IBug FindBugByTitle(string title)
        {
            return this.AllBugs.SingleOrDefault(b => b.Title == title);
        }

        public IFeedback FindFeedbackById(int id)
        {
            return this.AllFeedbacks.SingleOrDefault(f => f.Id == id);
        }

        public IFeedback FindFeedbackByTitle(string title)
        {
            return this.AllFeedbacks.SingleOrDefault(f => f.Title == title);
        }

        public IStory FindStoryById(int id)
        {
            return this.AllStories.SingleOrDefault(s => s.Id == id);
        }

        public IStory FindStoryByTitle(string title)
        {
            return this.AllStories.SingleOrDefault(s => s.Title == title);
        }
    }
}
