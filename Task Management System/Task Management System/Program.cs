﻿using System;
using Task_Management_System.Core;

namespace Task_Management_System
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var repository = new Repository();
            var commandFactory = new CommandFactory(repository);
            var engine = new Engine(commandFactory);
            engine.Start();
        }
    }
}
