﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_Management_System.Commands;

namespace Task_Management_System.Helpers
{
    public static class ValidationMethods
    {
        public static IFormatProvider StringOutOfRange { get; private set; }

        public static void ValidateStringLength(int valueLength, int minValue, int maxValue, string fieldName)
        {
            if (valueLength < minValue || valueLength > maxValue)
            {
                throw new InvalidUserInputException(String.Format(Constants.StringOutOfRange, fieldName, minValue, maxValue));
            }
        }

        public static void ValidateNotNull(object toValidate, string fieldName)
        {
            if (toValidate is null)
            {
                throw new ArgumentNullException(string.Format(Constants.ObjectIsNull, fieldName));
            }
        }

        public static void ValidateIntValue(int value, int minValue, int maxValue, string fieldName)
        {
            if (value < minValue || value > maxValue)
            {
                throw new InvalidUserInputException(String.Format(Constants.ValueOutOfRange, fieldName, minValue, maxValue));
            }
        }

    }
}
