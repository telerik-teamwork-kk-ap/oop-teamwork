﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Management_System.Helpers
{
    internal static class Constants
    {
        // Error messages
        public const string ValueOutOfRange = "{0} must be between {1} and {2}."; // {0} is the name of the field
        public const string StringOutOfRange = "{0} must be between {1} and {2} characters long."; // {0} is the name of the field
        public const string ObjectIsNull = "{0} can't be null."; // {0} is the name of the field
        public const string EntityNotFound = "{0} with name {1} was not found."; // {0} is the entity name
        // Error messages

        // 0 is the type of the object, 1 is its name.
        internal const string ObjectCreated = "{0} with name '{1}' was created."; 
        internal const string ObjectExists = "A {0} with name '{1}' already exists!";
        internal const string ObjectDoesNotExist = "A {0} with name {1} does not exist.";

        // 0 is the name of the command, 1 is the number of expected parameters, 2 is the count of received parameters
        internal const string InvalidNumberOfArguments = "Invalid number of arguments for '{0}'. Expected: {1}, Received: {2}.";
        internal const string InvalidNumberOfArgumentsForMoreComplexedCommands = "Invalid number of arguments for '{0}'. Type '<command name> / help' for help.";

        // {0} = propertyName; {1} = task ID; {2} = currentState; {3} = nextState;
        internal const string ChangeObjectProperty= "{0} of task with ID {1} was changed from {2} to {3}.";
        // {0} = task ID; {1} = memberName
        internal const string TaskReceivesAssignee = "Task with ID {0} was assigned to {1}.";
        internal const string TaskWasUnassigned = "Task with ID {0} was unassigned.";
        internal const string TaskReceivesComment = "A new comment has been added to task with ID {0}.";
    }
}
