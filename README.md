## Task Management System
#### A small console app created by Kaloyan and Anna during the first Telerik OOP Teamwork.

## Description
### This is a console application made for a small team of programmers to manage their tasks and it supports many commands.
##### This Project meets the requirements listed on the learn platform and there are some extra features too:
    - help command to list all supported commands
    - help parameter for all commands that take in parameters to show the syntax of the command
    - fresh new color theme

## Authors and acknowledgment
#### Authors:
    - Anna Petrova
    - Kaloyan Kraynin

## Project status
#### Project is mostly finished and working. There is some minor changes we need to make.
